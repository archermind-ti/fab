# Floating Action Button Library for ohos

### 项目介绍

功能：浮动操作按钮的实现, 以浮动在 UI 上方的圆圈图标为特征，并具有与变形、启动和转移锚点相关的特殊运动行为。

###  安装教程

方式一：

1. 下载fab模块代码添加到自己的工程

2. 在需要使用的模块中关联使用

   ```
   dependencies {
       implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
       implementation project(':fab')
       testImplementation 'junit:junit:4.13'
   	……
   }
   ```

3. gradle sync

方案二：

        allprojects{
            repositories{
                mavenCentral()
            }
        }
        
        implementation 'com.gitee.archermind-ti:fab:1.0.1'

### Demo

#### Button Type：

浮动操作按钮有 3 种尺寸：

- DEFAULT (56vp) - used in most cases

- MINI (40vp) - used to create visual continuity with other elements on the screen

- BIG (72vp) - additional button size, useful for large screens (not covered by Material Design)

|                      DEFAULT                      |                      MINI                      |                      BIG                      |
| :-----------------------------------------------: | :--------------------------------------------: | :-------------------------------------------: |
| <img src="/demo/default.jpg" width="33%" /> | <img src="/demo/mini.jpg" width="33%" /> | <img src="/demo/big.jpg" width="33%" /> |

#### Color palette

该库包含Material Color Palette 500和900色彩

|                    Green 500                    |                    Amber 500                    |                    Blue 500                    |
| :---------------------------------------------: | :---------------------------------------------: | :--------------------------------------------: |
| <img src="/demo/green.jpg" width="33%" /> | <img src="/demo/yello.jpg" width="33%" /> | <img src="/demo/blue.jpg" width="33%" /> |

#### Shadow

默认情况下启用阴影，可以通过三种方式修改阴影：半径、X 或 Y 轴偏移和颜色。

|                      Default                       |                       Radius                       |               X- and Y- axis offiset               |
| :------------------------------------------------: | :------------------------------------------------: | :------------------------------------------------: |
| <img src="/demo/shadow_d.jpg" width="33%" /> | <img src="/demo/shadow_r.jpg" width="33%" /> | <img src="/demo/shadow_x.jpg" width="33%" /> |

#### Stroke

默认情况下禁用stroke。 可以通过两种方式修改：宽度和颜色。

|                        Thin                        |                       Medium                       |                       Thick                        |
| :------------------------------------------------: | :------------------------------------------------: | :------------------------------------------------: |
| <img src="/demo/stroke_t.jpg" width="33%" /> | <img src="/demo/stroke_2.jpg" width="33%" /> | <img src="/demo/stroke_3.jpg" width="33%" /> |

#### Effects

- Shadow Responsive Effect - 阴影响应效果意味着阴影随用户输入而改变。 默认启用阴影响应效果。

- Ripple Effect - 触摸水波效果，在ActionButton内收到触摸事件时发生。默认不使用。


| Shadow Responsive Effect                        | Ripple Effect                                    |
| :---------------------------------------------- | ------------------------------------------------ |
| <img src="/demo/press.gif" width="33%" /> | <img src="/demo/ripple.gif" width="33%" /> |

#### Moving

ActionButton 可以向各方向移动

| Left-Right Move                                     | Up -Down Move                                     |
| :-------------------------------------------------- | :------------------------------------------------ |
| <img src="/demo/move_left.gif" width="33%" /> | <img src="/demo/move_up.gif" width="33%" /> |

#### Animations

该库有几个预定义动画：

|               Fade In - Fade Out               |            Roll From Down - Roll To Down            |            Jump From Down - Jump To Down            |
| :--------------------------------------------: | :-------------------------------------------------: | :-------------------------------------------------: |
| <img src="/demo/fade.gif" width="33%" /> | <img src="/demo/roll_down.gif" width="33%" /> | <img src="/demo/jump_down.gif" width="33%" /> |

|              Scale In - Scale Out               |           Roll From Right - Roll To Right            |           Jump From Right - Jump To Right            |
| :---------------------------------------------: | :--------------------------------------------------: | :--------------------------------------------------: |
| <img src="/demo/scale.gif" width="33%" /> | <img src="/demo/roll_right.gif" width="33%" /> | <img src="/demo/jump_right.gif" width="33%" /> |



### 使用方法：

#### Create

ActionButton 按钮可以在 XML 资源中声明。

```
<DependentLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    xmlns:fab="http://schemas.huawei.com/res/ohos-auto"
    ohos:height="match_parent"
    ohos:width="match_parent">

    <com.scalified.fab.ActionButton
        ohos:id="$+id:action_button"
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:align_parent_bottom="true"
        ohos:align_parent_right="true"
        ohos:right_margin="$string:fab_margin"
        ohos:bottom_margin="$string:fab_margin"
        fab:type="DEFAULT" />
</DependentLayout>
```

```
// And then find it within the content view:
actionButton = (ActionButton)findComponentById(ResourceTable.Id_action_button);
```
> 没有必需的配置参数。 所有配置参数都是可选的。

ActionButton也可以在代码中声明

```
ActionButton actionButton = new ActionButton(getContext());
// And then add it to the content view
```

#### Button actions

-  Showing and Hiding

```
actionButton.show();    // shows the button if it is hidden and plays the show animation if set
actionButton.hide();    // hides the button if it is shown and plays the hide animation if set
actionButton.dismiss(); // completely dismisses the button and plays the hide animation if set
```

> dissmiss后，按钮从其父视图中完全删除，因此再使用show等操作将无效

button的当前状态可以通过下面API判断

```
boolean shown = actionButton.isShown();
boolean hidden = actionButton.isHidden();
boolean dismissed = actionButton.isDismissed();
```

-  Playing animations

在某些情况下需要在不调用 show()、hide() 或dismiss() 方法的情况下强制播放动画，可使用：

```
actionButton.playShowAnimation();   // plays the show animation
actionButton.playHideAnimation();   // plays the hide animation
```
> 动画仅在设置时播放。 默认情况下，未设置动画。

-  Moving ActionButton

ActionButton可以在其父容器内移动

```
// Initialize the moving distance
int distance = 100.0f // in virtual pixels (vp)

// Move ActionButton left
actionButton.moveLeft(distance);

// Move ActionButton up
actionButton.moveUp(distance);

// Move ActionButton right
actionButton.moveRight(distance);

// Move ActionButton down
actionButton.moveDown(distance);
```

#### 定制

- Button Type

  该库有三种尺寸的Button: DEFAULT(56vp)、MINI(40vp)、BIG(72vp)。

  更改/获取Type：

  ```
  // To set the button type:
  actionButton.setType(ActionButton.Type.MINI);
  
  // To get the button type:
  ActionButton.Type type = actionButton.getType();
  ```

- Button Size

  ```
  // To get the ActionButton size
  float size = actionButton.getSize();
  
  // To set the ActionButton size
  actionButton.setSize(60.0f); // in virtual pixels (vp)
  ```

-  Button states

  支持两种按钮状态：NORMAL、PRESSED

  ```
  // To set the button state:
  actionButton.setState(ActionButton.State.PRESSED);
  
  // To get the button state:
  ActionButton.State state = actionButton.getState();
  ```

- Button colors

  可以为按钮的NORMAL、PRESSED状态设置颜色，默认设置：**#FF9B9B9B**（NORMAL)、**#FF696969**(PRESSED)

  相关API

  ```
  // To set button color for normal state:
  actionButton.setButtonColor(getResourceManager().getElement(ResourceTable.Color_fab_material_lime_500).getColor())
  
  // To get button color for normal state:
  int buttonColor = actionButton.getButtonColor();
  
  // To set button color for pressed state:
  actionButton.setButtonColorPressed(getResourceManager().getElement(ResourceTable.Color_fab_material_lime_900).getColor())
  
  // To get button color for pressed state:
  int buttonColorPressed = actionButton.getButtonColorPressed();
  ```

- Shadow

  阴影可以通过三种方式自定义：颜色、半径和偏移。 默认启用阴影并具有以下默认值：

  - **shadowColor** = **#42000000** (~ light-grey)
  - **shadowRadius** = **8.0f** ( in virtual pixels (vp) )
  - **shadowXOffset** = **0.0f** ( in virtual pixels (vp) )
  - **shadowYOffset** = **8.0f** ( in virtual pixels (vp) )

  相关API

  ```
  // To check whether shadow is present:
  boolean hasShadow = actionButton.hasShadow();
  
  // To set the shadow color:
  actionButton.setShadowColor(getResourceManager().getElement(ResourceTable.Color_fab_material_grey_500));
  
  // To get the shadow color:
  int shadowColor = actionButton.getShadowColor();
  
  // To set the shadow radius:
  actionButton.setShadowRadius(5.0f);
  
  // To get the shadow radius:
  float shadowRadius = actionButton.getShadowRadius();
  
  // To set the shadow X-axis offset:
  actionButton.setShadowXOffset(3.5f);
  
  // To get the shadow X-axis offset:
  float shadowXOffset = actionButton.getShadowXOffset();
  
  // To set the shadow Y-axis offset:
  actionButton.setShadowYOffset(3.0f);
  
  // To get the shadow Y-axis offset:
  float shadowYOffset = actionButton.getShadowYOffset();
  
  // To remove shadow:
  actionButton.removeShadow();
  ```

- Image

  Action Button 可以有一个居中的图像。 默认情况下图像不存在。

  相关API：

  ```
  // To check whether image is present:
  boolean hasImage = actionButton.hasImage();
  
  // To set an image (either pixelMap, pixelMapElement or resource id):
  actionButton.setImagePixelMap(pixelMap);
  actionButton.setImageElement(pixelMapElement);
  actionButton.setImageResource(ResourceTable.Media_fab_plus_icon);
  
  // To get an image:
  PixelMap pixelMap = actionButton.getImage();
  
  // To set the image size (which is by default 24.0vp):
  actionButton.setImageSize(30.0); //in virtual pixels (vp)
  
  // To get the image size (in actual pixels (px))
  float imageSize = actionButton.getImageSize();
  
  // To remove the image:
  actionButton.removeImage();
  ```

- Stroke

  stroke可以通过两种方式进行自定义：宽度和颜色。 默认情况下禁用，但它具有默认值：

  - **strokeColor** = **Color.BLACK.getValue()**
  - **strokeWidth** = **0.0f** (no stroke)

  相关API

  ```
  // To check whether stroke enabled:
  boolean hasStroke = actionButton.hasStroke();
  
  // To set stroke color:
  actionButton.setStrokeColor(getResourceManager().getElement(ResourceTable.Color_fab_material_blue_grey_500));
  
  // To get stroke color:
  int strokeColor = actionButton.getStrokeColor();
  
  // To set stroke width:
  actionButton.setStrokeWidth(1.5f);
  
  // To get stroke width:
  float strokeWidth = actionButton.getStrokeWidth();
  
  // To remove the stroke:
  actionButton.removeStroke();
  ```

- Effects

  -  Shadow Responsive Effect

    阴影响应效果意味着阴影随用户输入而改变。 当用户点击按钮并按住它时，阴影半径增加到一定值。 然后，当用户释放按钮时，阴影半径减小到其初始值。 默认启用阴影响应效果

    ```
    // To check whether Shadow Responsive Effect enabled:
    boolean shadowResponsiveEffectEnabled = actionButton.isShadowResponsiveEffectEnabled();
    
    //To enable or disable Shadow Responsive Effect:
    actionButton.setShadowResponsiveEffectEnabled(true);
    actionButton.setShadowResponsiveEffectEnabled(false);
    ```

    

  -  Ripple Effect

    默认情况下禁用波纹效果。 默认效果下，波纹颜色是 PRESSED 状态颜色的暗变体

    ```
    // To check whether Ripple Effect enabled:
    boolean rippleEffectEnabled = actionButton.isRippleEffectEnabled();
    
    // To enable or disable Ripple Effect:
    actionButton.setRippleEffectEnabled(true);
    actionButton.setRippleEffectEnabled(false);
    
    // To set button color ripple:
    actionButton.setButtonColorRipple(getResourceManager().getElement(ResourceTable.Color_fab_material_grey_900));
    
    // To get button color ripple:
    int buttonColorRipple = actionButton.getButtonColorRipple();
    ```

- Animations

  Action Button 支持 2 种动画类型：动画在显示按钮时播放，动画在隐藏按钮时播放。 默认情况下，既不显示动画，也不设置隐藏动画。

  ```
  // To set show animation:
  actionButton.setShowAnimation(ActionButton.Animations.FADE_IN);
  actionButton.setShowAnimation(animatorGroup)
  
  // To get show animation:
  AnimatorGroup animatorGroup = actionButton.getShowAnimation();
  
  // To remove show animation:
  actionButton.removeShowAnimation();
  
  // To set hide animation:
  actionButton.setHideAnimation(ActionButton.Animations.FADE_OUT);
  actionButton.setHideAnimation(animatorGroup)
  
  // To get hide animation:
  AnimatorGroup animatorGroup = actionButton.getHideAnimation();
  
  // To remove hide animation:
  actionButton.removeHideAnimation();
  ```

  

###  版本迭代

- v1.0.0

###  版权和许可信息

```
Copyright 2016 Scalified <http://www.scalified.com>

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
```

