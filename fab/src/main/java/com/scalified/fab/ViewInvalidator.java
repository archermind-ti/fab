package com.scalified.fab;

import ohos.agp.components.Component;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * Used to invalidate the view
 * <p>
 * Contains invalidation options, which can be configured and then
 * used for view invalidation
 */
public class ViewInvalidator {
    static final HiLogLabel TAG = new HiLogLabel(HiLog.LOG_APP, 0, "ViewInvalidator");

    /**
     * Indicates whether invalidation required
     */
    private boolean invalidationRequired;

    /**
     * Indicates whether delayed invalidation required
     */
    private boolean invalidationDelayedRequired;

    /**
     * Delay time in ms used for delayed invalidation
     */
    private long invalidationDelay;

    /**
     * A view to invalidate
     */
    private final Component view;

    private Context context;

    /**
     * Creates the {@link ViewInvalidator} instance
     *
     * @param view view to be invalidate
     */
    ViewInvalidator(Context context, Component view) {
        this.context = context;
        this.view = view;
    }

    /**
     * Returns whether invalidation required
     *
     * @return true if invalidation required, otherwise false
     */
    boolean isInvalidationRequired() {
        return invalidationRequired;
    }

    /**
     * Sets that invalidation is required
     *
     */
    void requireInvalidation() {
        this.invalidationRequired = true;
        HiLog.info(TAG,"Set invalidation required");
    }

    /**
     * Returns whether delayed invalidation required
     *
     * @return true if delayed invalidation required, otherwise false
     */
    boolean isInvalidationDelayedRequired() {
        return invalidationDelayedRequired;
    }

    /**
     * Sets that delayed invalidation required
     */
    void requireDelayedInvalidation() {
        this.invalidationDelayedRequired = true;
        HiLog.info(TAG,"Set delayed invalidation required");
    }

    /**
     * Returns the invalidation delay time in ms used for delayed invalidation
     *
     * @return invalidation delay time in ms used for delayed invalidation
     */
    long getInvalidationDelay() {
        return invalidationDelay;
    }

    /**
     * Sets the invalidation delay time used for delayed invalidation
     *
     * @param invalidationDelay invalidation delay time in ms
     */
    void setInvalidationDelay(long invalidationDelay) {
        this.invalidationDelay = invalidationDelay;
    }

    /**
     * Invalidates the view based on the current invalidator configuration
     */
    void invalidate() {
        if (isInvalidationRequired()) {
            context.getUITaskDispatcher().asyncDispatch(new Runnable() {
                @Override
                public void run() {
                    view.invalidate();
                }
            });
            HiLog.info(TAG,"Called view invalidation");
        }
        if (isInvalidationDelayedRequired()) {
            context.getUITaskDispatcher().delayDispatch(new Runnable() {
                @Override
                public void run() {
                    view.invalidate();
                }
            },getInvalidationDelay());
            HiLog.info(TAG,"Called view delayed invalidation. Delay time is: "+ getInvalidationDelay());
        }
        reset();
    }

    /**
     * Resets the current invalidator configuration
     */
    private void reset() {
        invalidationRequired = false;
        invalidationDelayedRequired = false;
        setInvalidationDelay(0L);
        HiLog.info(TAG,"Reset the view invalidator configuration");
    }
}
