package com.scalified.fab;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorGroup;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.*;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.BlurDrawLooper;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.global.resource.Resource;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.multimodalinput.event.TouchEvent;


public class ActionButton extends Component implements Component.DrawTask, Component.TouchEventListener {
    static final HiLogLabel TAG = new HiLogLabel(HiLog.LOG_APP, 0, "ActionButton");

    /**
     * <b>Action Button</b> type
     */
    private Type type = Type.DEFAULT;

    /**
     * <b>Action Button</b> size in actual pixels
     */
    private float size = vpToPx(type.getSize());

    /**
     * <b>Action Button</b> state
     */
    private State state = State.NORMAL;

    /**
     * <b>Action Button</b> color for the {@link State#NORMAL} state
     */
    private int buttonColor = Color.getIntColor("#FF9B9B9B");

    /**
     * <b>Action Button</b> color for the {@link State#PRESSED} state
     */
    private int buttonColorPressed = Color.getIntColor("#FF696969");

    /**
     * Determines whether <b>Action Button</b> Ripple Effect enabled or not
     */
    private boolean rippleEffectEnabled;

    /**
     * <b>Action Button</b> Ripple Effect color
     */
    private int buttonColorRipple = darkenButtonColorPressed();

    /**
     * Shadow radius expressed in actual pixels
     */
    private float shadowRadius = vpToPx(8.0f);

    /**
     * Shadow X-axis offset expressed in actual pixels
     */
    private float shadowXOffset = vpToPx(0.0f);

    /**
     * Shadow Y-axis offset expressed in actual pixels
     */
    private float shadowYOffset = vpToPx(8.0f);

    /**
     * Shadow color
     */
    private int shadowColor = Color.getIntColor("#42000000");

    /**
     * Determines whether Shadow Responsive Effect enabled
     * <p>
     * Responsive Shadow means that shadow is enlarged up to the certain limits
     * while in the {@link ActionButton.State#PRESSED} state
     */
    private boolean shadowResponsiveEffectEnabled = true;

    /**
     * Stroke width
     */
    private float strokeWidth = 0.0f;

    /**
     * Stroke color
     */
    private int strokeColor = Color.BLACK.getValue();

    /**
     * <b>Action Button</b> image drawable centered inside the view
     */
    private PixelMap image;

    /**
     * Size of the <b>Action Button</b> image inside the view
     */
    private float imageSize = vpToPx(24.0f);

    /**
     * Animation, which is used while showing <b>Action Button</b>
     */
    private AnimatorGroup  showAnimation;

    /**
     * Animation, which is used while hiding or dismissing <b>Action Button</b>
     */
    private AnimatorGroup hideAnimation;

    private AnimatorValue  moveAnimation;

    private Animations showAnimations;
    private Animations hideAnimations;

    private float startAnimationY;
    private float startAnimationX;

    /**
     * <b>Action Button</b> touch point
     * <p>
     * TouchPoint contains information about X- and
     * Y-axis touch points within the <b>Action Button</b>
     */
    private TouchPoint touchPoint = new TouchPoint(0.0f, 0.0f);

    /**
     * which is used for drawing the elements of
     * <b>Action Button</b>
     */
    private final Paint paint = new Paint() {
        {
            setAntiAlias(true);
        }
    };

    /**
     * A view invalidator, which is used to invalidate the <b>Action Button</b>
     */
    private final ViewInvalidator invalidator = new ViewInvalidator(getContext(),this);

    /**
     * A drawer, which is used for drawing the <b>Action Button</b> Ripple Effect
     */
    private final EffectDrawer rippleEffectDrawer = new RippleEffectDrawer(this);

    /**
     * A drawer, which is used for drawing the <b>Action Button</b> Shadow Responsive Effect
     */
    private final EffectDrawer shadowResponsiveDrawer = new ShadowResponsiveDrawer(this);

    /**
     * A view mover, which is used to move the <b>Action Button</b>
     */
    //protected final ViewMover mover = ViewMoverFactory.createInstance(this);
    private int hasDraw = 0;


    public ActionButton(Context context) {
        this(context, null);
    }

    public ActionButton(Context context, AttrSet attrs) {
        this(context, attrs, null);
    }

    public ActionButton(Context context, AttrSet attrs, String styleName) {
        super(context, attrs, styleName);
        //initActionButton();
        initActionButtonAttrs(context, attrs, styleName);
    }

    /**
     * Initializes the <b>Action Button</b>, which is created programmatically
     */
    private void initActionButton() {
        initLayerType();
        HiLog.info(TAG, "Initialized the Action Button");
    }

    /**
     * Initializes the <b>Action Button</b> attributes, declared within XML resource
     * <p>
     * Makes calls to different initialization methods for parameters initialization.
     * For those parameters, which are not declared in the XML resource,
     * the default value will be used
     *
     * @param context context the view is running in
     * @param attrs attributes of the XML tag that is inflating the view
     * @param defStyleAttr attribute in the current theme that contains a
     *        reference to a style resource that supplies default values for
     *        the view. Can be 0 to not look for defaults
     */
    private void initActionButtonAttrs(Context context, AttrSet attrs, String defStyleAttr) {
        try {
            initType(attrs);
            initSize(attrs);
            initButtonColor(attrs);
            initButtonColorPressed(attrs);
            initRippleEffectEnabled(attrs);
            initButtonColorRipple(attrs);
            initShadowRadius(attrs);
            initShadowXOffset(attrs);
            initShadowYOffset(attrs);
            initShadowColor(attrs);
            initShadowResponsiveEffectEnabled(attrs);
            initStrokeWidth(attrs);
            initStrokeColor(attrs);
            initImage(attrs);
            initImageSize(attrs);
            initShowAnimation(attrs);
            initHideAnimation(attrs);
            initMoveAnimation(attrs);
        } catch (Exception e) {
            HiLog.info(TAG, "Failed to read attribute", e);
        }

        getInvalidator().requireInvalidation();
        setClickable(true);
        addDrawTask(this);
        setTouchEventListener(this);
    }

    /**
     * Initializes the layer type needed for shadows drawing
     * <p>
     *
     */
    private void initLayerType() {
        //setLayerType(LAYER_TYPE_SOFTWARE, getPaint());
        HiLog.info(TAG,"Initialized the layer type");
    }

    /**
     * Initializes the Type of <b>Action Button</b>
     * <p>
     * Must be called before initSize for the proper
     * <b>Action Button</b> size initialization
     *
     *
     */
    private void initType(AttrSet attrSet) {
        String str = AttrUtils.getStringFromAttr(attrSet, "type", Type.DEFAULT.getString());
        type = Type.forString(str);
        HiLog.info(TAG,"Initialized Action Button type: "+str);
    }

    private void initSize(AttrSet attrSet) {
        //this.size =  vpToPx(AttrUtils.getDimensionFromAttr(attrSet, "size", type.getSize()));
        this.size =  AttrUtils.getDimensionFromAttr(attrSet, "size", type.getSize());
        HiLog.info(TAG,"Initialized Action Button size: "+ getSize());
    }

    /**
     * Initializes the <b>Action Button</b> color for the {@link State#NORMAL}
     * {@link #state}
     *
     * @param attrSet attributes of the XML tag that is inflating the view
     */
    private void initButtonColor(AttrSet attrSet) {
        buttonColor = AttrUtils.getColorFromAttr(attrSet, "button_color", buttonColor);
        HiLog.info(TAG,"Initialized Action Button color: "+ getButtonColor());
    }

    private void initButtonColorPressed(AttrSet attrSet) {
        buttonColorPressed = AttrUtils.getColorFromAttr(attrSet, "button_color_pressed", buttonColorPressed);
        buttonColorRipple = darkenButtonColorPressed();
        HiLog.info(TAG,"Initialized Action Button color pressed: "+ getButtonColorPressed());
    }

    /**
     * Initializes the <b>Action Button</b> Ripple Effect state
     *
     * param attrs attributes of the XML tag that is inflating the view
     */
    private void initRippleEffectEnabled(AttrSet attrSet) {
        rippleEffectEnabled = AttrUtils.getBooleanFromAttr(attrSet, "rippleEffect_enabled", rippleEffectEnabled);
        HiLog.info(TAG, "Initialized Action Button Ripple Effect enabled:" + isRippleEffectEnabled());
    }

    private void initButtonColorRipple(AttrSet attrSet) {
        buttonColorRipple = AttrUtils.getColorFromAttr(attrSet, "button_colorRipple", buttonColorRipple);
        HiLog.info(TAG, "Initialized Action Button Ripple Effect color: " + getButtonColorRipple());
    }

    private void initShadowRadius(AttrSet attrSet) {
        shadowRadius = AttrUtils.getDimensionFromAttr(attrSet, "shadow_radius", shadowRadius);
        HiLog.info(TAG, "Initialized Action Button shadow radius: " + getShadowRadius());
    }

    private void initShadowXOffset(AttrSet attrSet) {
        shadowXOffset = AttrUtils.getDimensionFromAttr(attrSet, "shadow_xoffset", shadowXOffset);
        HiLog.info(TAG, "Initialized Action Button X-axis offset: " + getShadowXOffset());
    }

    private void initShadowYOffset(AttrSet attrSet) {
        shadowYOffset = AttrUtils.getDimensionFromAttr(attrSet, "shadow_yoffset", shadowYOffset);
        HiLog.info(TAG, "Initialized Action Button Y-axis offset: " + getShadowYOffset());
    }

    private void initShadowColor(AttrSet attrSet) {
        shadowColor = AttrUtils.getColorFromAttr(attrSet, "shadow_color", shadowColor);
        HiLog.info(TAG, "Initialized Action Button shadow color: "+getShadowColor());
    }

    /**
     * Initializes the Shadow Responsive Effect
     *
     * @param attrSet attributes of the XML tag that is inflating the view
     */
    private void initShadowResponsiveEffectEnabled(AttrSet attrSet) {
        shadowResponsiveEffectEnabled = AttrUtils.getBooleanFromAttr(attrSet, "shadowResponsiveEffect_enabled", shadowResponsiveEffectEnabled);
        HiLog.info(TAG, "Initialized Action Button Shadow Responsive Effect enabled: " + isShadowResponsiveEffectEnabled());
    }

    /**
     * Initializes the stroke width
     *
     * @param attrSet attributes of the XML tag that is inflating the view
     */
    private void initStrokeWidth(AttrSet attrSet) {
        strokeWidth = AttrUtils.getDimensionFromAttr(attrSet, "stroke_width", strokeWidth);
        HiLog.info(TAG, "Initialized Action Button stroke width: "+getStrokeWidth());
    }

    /**
     * Initializes the stroke color
     *
     * @param attrSet attributes of the XML tag that is inflating the view
     */
    private void initStrokeColor(AttrSet attrSet) {
        strokeColor = AttrUtils.getColorFromAttr(attrSet, "stroke_color", strokeColor);
        HiLog.info(TAG, "Initialized Action Button stroke width: "+getStrokeWidth());
    }

    private void initShowAnimation(AttrSet attrSet) {

    }

    private void initHideAnimation(AttrSet attrSet) {

    }

    private void initMoveAnimation(AttrSet attrSet) {
        moveAnimation = new AnimatorValue();
        moveAnimation.setDuration(300);
    }

    /**
     * Initializes the image inside <b>Action Button</b>
     *
     * @param attrSet attributes of the XML tag that is inflating the view
     */
    private void initImage(AttrSet attrSet) {
        int resId = AttrUtils.getIntFromAttr(attrSet, "image", 0);
        if (resId != 0) {
            setImageResource(resId);
        }
    }

    /**
     * Initializes the image inside <b>Action Button</b>
     *
     * * Must be specified in virtual pixels (vp) , which are
     *      * then converted into actual pixels (px).
     *
     * @param attrSet attributes of the XML tag that is inflating the view
     */
    private void initImageSize(AttrSet attrSet) {
        //imageSize = vpToPx(AttrUtils.getDimensionFromAttr(attrSet, "image_size", pxToVp(imageSize)));
        imageSize = AttrUtils.getDimensionFromAttr(attrSet, "image_size", pxToVp(imageSize));
    }

    private float pxToVp(float px) {
        int density = getResourceManager().getDeviceCapability().screenDensity/160;
        return px/density;
    }

    public void playShowAnimation() {
        HiLog.info(TAG, "playShowAnimation");
        startAnimation(getShowAnimation());
    }

    public void playHideAnimation() {
        HiLog.info(TAG, "playHideAnimation");
        if (getHideAnimation() != null) {
            startAnimation(getHideAnimation());
        } else {
            HiLog.info(TAG, "animation null, hide");
            setVisibility(INVISIBLE);
        }
    }

    public void startAnimation(AnimatorGroup animation) {
        if (animation != null ) {
            animation.start();
            HiLog.info(TAG, "animation start");
        }
    }

    public void show() {
        if (isHidden()) {
            playShowAnimation();
            setVisibility(VISIBLE);

            HiLog.info(TAG,"Shown the Action Button");
        }
    }

    public void hide() {
        if (!isHidden() && !isDismissed()) {
            playHideAnimation();
            //setVisibility(INVISIBLE);
            HiLog.info(TAG,"Hidden the Action Button");
        }
    }

    public void dismiss() {
        if (!isDismissed()) {
            if (!isHidden()) {
                playHideAnimation();
            }
            setVisibility(INVISIBLE);
            ComponentParent parent = getComponentParent();
            parent.removeComponent(this);
            HiLog.info(TAG, "Dismissed the Action Button");
        }
    }

    public boolean isHidden() {
        return getVisibility() == INVISIBLE;
    }

    public boolean isDismissed() {
        return getComponentParent() == null;
    }

    public boolean isShown() {
        return getVisibility() == VISIBLE;
    }

//    public void move(MovingParams params) {
//
//    }

    public void moveRight(float distance) {
        float newDisance = vpToPx(distance);
        if (null != moveAnimation && !moveAnimation.isRunning()) {
            float x = getContentPositionX();
            if (hasHorizontalSpaceToMove(newDisance)) {
                moveAnimation.cancel();
                moveAnimation.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                    @Override
                    public void onUpdate(AnimatorValue animatorValue, float v) {
                        setContentPosition(x+v*newDisance, getContentPositionY());
                    }
                });
                moveAnimation.start();
            }

        }

    }

    public void moveLeft(float distance) {
        float newDisance = vpToPx(distance);
        if (null != moveAnimation && !moveAnimation.isRunning()) {
            float x = getContentPositionX();
            if (hasHorizontalSpaceToMove(-newDisance)) {
                moveAnimation.cancel();
                moveAnimation.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                    @Override
                    public void onUpdate(AnimatorValue animatorValue, float v) {
                        setContentPosition(x-v*newDisance, getContentPositionY());
                    }
                });
                moveAnimation.start();
            }

        }

    }

    public void moveDown(float distance) {
        float newDisance = vpToPx(distance);
        if (null != moveAnimation && !moveAnimation.isRunning()) {
            float y = getContentPositionY();
            if (hasVerticalSpaceToMove(newDisance)) {
                moveAnimation.cancel();
                moveAnimation.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                    @Override
                    public void onUpdate(AnimatorValue animatorValue, float v) {
                        setContentPosition(getContentPositionX(), y+v*newDisance);
                    }
                });
                moveAnimation.start();
            }

        }

    }

    public void moveUp(float distance) {
        float newDisance = vpToPx(distance);
        if (null != moveAnimation && !moveAnimation.isRunning()) {
            float y = getContentPositionY();
            if (hasVerticalSpaceToMove(-newDisance)) {
                moveAnimation.cancel();
                moveAnimation.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                    @Override
                    public void onUpdate(AnimatorValue animatorValue, float v) {
                        setContentPosition(getContentPositionX(), y-v*newDisance);
                    }
                });
                moveAnimation.start();
            }

        }

    }


    private boolean hasHorizontalSpaceToMove(float xAxisDelta) {
        boolean ret;
        ComponentContainer parent = (ComponentContainer)getComponentParent();
        int parentWidth = parent.getWidth();
        HiLog.info(TAG, "hasHorizontalSpaceToMove, to move "+ xAxisDelta+", parent width = "+parentWidth);
        if (xAxisDelta < 0) {
            //move left
            int endLeftBound = calculateEndLeftBound(xAxisDelta);
            ret = endLeftBound>=0;
        } else {
            int endRightBound = calculateEndRightBound(xAxisDelta);
            ret = endRightBound <= parentWidth;
        }

        HiLog.info(TAG, "hasHorizontalSpaceToMove, ret = "+ret);
        return ret;
    }


    private boolean hasVerticalSpaceToMove(float yAxisDelta) {
        boolean ret;
        ComponentContainer parent = (ComponentContainer)getComponentParent();
        int parentHeight = parent.getHeight();
        HiLog.info(TAG, "hasHorizontalSpaceToMove, to move "+ yAxisDelta+", parent height = "+parentHeight);
        int endTopBound = calculateEndTopBound(yAxisDelta);
        int endBottomBound = calculateEndBottomBound(yAxisDelta);
        if (yAxisDelta > 0) {
            //move down
            ret = endBottomBound <= parentHeight;
        } else {
            ret = endTopBound >= 0;
        }

        HiLog.info(TAG, "hasVerticalSpaceToMove, ret = "+ret);
        return ret;
    }

    private int calculateEndTopBound(float yAxisDelta) {
        return (int) (getContentPositionY() + yAxisDelta);
    }

    private int calculateEndBottomBound(float yAxisDelta) {
        return calculateEndTopBound(yAxisDelta) + getHeight();
    }

    private int calculateEndLeftBound(float xAxisDelta) {
        return (int) (getContentPositionX() + xAxisDelta);
    }

    private int calculateEndRightBound(float xAxisDelta) {
        return calculateEndLeftBound(xAxisDelta) + getWidth();
    }



    public void move(float distanceX, float distanceY) {
        float x = getContentPositionX();
        float y = getContentPositionY();
        float newDisanceX = vpToPx(distanceX);
        float newDisanceY = vpToPx(distanceY);
        if (null != moveAnimation) {
            moveAnimation.cancel();
            moveAnimation.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {
                    setContentPosition(x-v*newDisanceX, y-v*newDisanceY);
                }
            });
            moveAnimation.start();
        }
    }

    /**
     * Returns the type of the <b>Action Button</b>
     *
     * @return type of the <b>Action Button</b>
     */
    public Type getType() {
        return type;
    }

    /**
     * Sets the <b>Action Button</b> {@link ActionButton.Type}
     * and calls {@link #setSize(float)} to set the size depending on {@link #type} set
     *
     * @param type type of the <b>Action Button</b>
     */
    public void setType(Type type) {
        this.type = type;
        HiLog.info(TAG,"Changed the Action Button type to: "+ getType());
        setSize(getType().getSize());
    }

    @Deprecated
    public int getButtonSize() {
        return (int) getSize();
    }

    /**
     * Returns the size of the <b>Action Button</b> in actual pixels (px).
     * Size of the <b>Action Button</b> is the diameter of the main circle
     *
     * @return size of the <b>Action Button</b> in actual pixels (px)
     */
    public float getSize() {
        return size;
    }

    /**
     * Sets the <b>Action Button</b> size and invalidates the layout of the view
     *
     * Setting the button size explicitly means, that button types with its default sizes are
     * completely ignored. Do not use this method, unless you know what you are doing
     * <p>
     * Must be specified in virtual pixels (vp), which are
     * then converted into actual pixels (px)
     *
     * @param size size of the button specified in density-independent
     *                   (dp) pixels
     */
    public void setSize(float size) {
        this.size = vpToPx(size);
        onMeasure();

        //postLayout();
        invalidate();
        updateAnimationParam();

        HiLog.info(TAG, "Set the Action Button size to: "+getSize());
    }

    private void updateAnimationParam() {
        if (null != showAnimations || null != hideAnimations) {
            mContext.getUITaskDispatcher().delayDispatch(new Runnable() {
                @Override
                public void run() {
                    startAnimationX = getContentPositionX();
                    startAnimationY = getContentPositionY();
                    if (null != showAnimations) {
                        setShowAnimation(createAnimatorProperty(showAnimations));
                    }
                    if (null != hideAnimations) {
                        setHideAnimation(createAnimatorProperty(hideAnimations));
                    }
                }
            }, 500);
        }
    }

    /**
     * Returns the current state of the <b>Action Button</b>
     *
     * @return current state of the <b>Action Button</b>
     */
    public State getState() {
        return state;
    }

    /**
     * Sets the current state of the <b>Action Button</b> and
     * invalidates the view
     *
     * @param state new state of the <b>Action Button</b>
     */
    public void setState(State state) {
        this.state = state;
        invalidate();
        HiLog.info(TAG,"Changed the Action Button state to: "+ getState());
    }

    /**
     * Returns the <b>Action Button</b> color when in
     * {@link State#NORMAL} state
     *
     * @return <b>Action Button</b> color when in
     * {@link State#NORMAL} state
     */
    public int getButtonColor() {
        return buttonColor;
    }

    /**
     * Sets the <b>Action Button</b> color when in
     * {@link State#NORMAL} state and invalidates the view
     *
     * @param buttonColor <b>Action Button</b> color
     *                    when in {@link State#NORMAL} state
     */
    public void setButtonColor(int buttonColor) {
        this.buttonColor = buttonColor;
        HiLog.info(TAG,"Changed the Action Button color to: "+ getButtonColor());
        invalidate();
    }

    /**
     * Returns the <b>Action Button</b> color when in
     * {@link State#PRESSED} state
     *
     * @return <b>Action Button</b> color when in
     * {@link State#PRESSED} state
     */
    public int getButtonColorPressed() {
        return buttonColorPressed;
    }

    /**
     * Sets the <b>Action Button</b> color when in
     * {@link State#PRESSED} state and invalidates the view
     *
     * @param buttonColorPressed <b>Action Button</b> color
     *                           when in {@link State#PRESSED} state
     */
    public void setButtonColorPressed(int buttonColorPressed) {
        this.buttonColorPressed = buttonColorPressed;
        setButtonColorRipple(darkenButtonColorPressed());
        HiLog.info(TAG, "Changed the Action Button color pressed to: " + getButtonColorPressed());
    }

    private int darkenButtonColorPressed() {
        float darkenFactor = 0.8f;
        return ColorModifier.modifyExposure(getButtonColorPressed(), darkenFactor);

    }

    /**
     * Checks whether <b>Action Button</b> Ripple Effect enabled
     *
     * @return true, if Ripple Effect enabled, otherwise false
     */
    public boolean isRippleEffectEnabled() {
        return rippleEffectEnabled;
    }

    /**
     * Toggles the Ripple Effect state
     *
     * @param enabled true if Ripple Effect needs to be enabled, otherwise false
     */
    public void setRippleEffectEnabled(boolean enabled) {
        this.rippleEffectEnabled = enabled;
        HiLog.info(TAG, "the Action Button Ripple Effect: " + (isRippleEffectEnabled()? "Enabled" : "Disabled"));
    }

    /**
     * Returns the <b>Action Button</b> Ripple Effect color
     *
     * @return <b>Action Button</b> Ripple Effect color
     */
    public int getButtonColorRipple() {
        return buttonColorRipple;
    }

    public void setButtonColorRipple(int buttonColorRipple) {
        this.buttonColorRipple = buttonColorRipple;
        HiLog.info(TAG, "Action Button Ripple Effect color changed to: " + getButtonColorRipple());
    }

    public boolean hasShadow() {
        //return !hasElevation() && getShadowRadius() > 0.0f;
        return true;
    }

    /**
     * Returns the <b>Action Button</b> shadow radius in actual
     * pixels (px)
     *
     * @return <b>Action Button</b> shadow radius in actual pixels (px)
     */
    public float getShadowRadius() {
        return shadowRadius;
    }

    /**
     * Sets the <b>Action Button</b> shadow radius and
     * invalidates the layout of the view
     * <p>
     * Must be specified in virtual pixels (vp) , which are
     * then converted into actual pixels (px). If shadow radius is set to 0,
     * shadow is removed
     * <p>
     * Additionally sets the {@link #shadowResponsiveDrawer} current radius
     * in case if Shadow Responsive Effect enabled
     *
     * @param shadowRadius shadow radius specified in virtual pixels (dp)
     *
     */
    public void setShadowRadius(float shadowRadius) {
        this.shadowRadius = vpToPx(shadowRadius);
        if (isShadowResponsiveEffectEnabled()) {
            ((ShadowResponsiveDrawer) shadowResponsiveDrawer).setCurrentShadowRadius(getShadowRadius());
        }
        invalidate();
        updateAnimationParam();
        HiLog.info(TAG,"Action Button shadow radius changed to: "+ getShadowRadius());
    }

    /**
     * Removes the <b>Action Button</b> shadow by setting its radius to 0
     */
    public void removeShadow() {
        if (hasShadow()) {
            setShadowRadius(0.0f);
        }
    }

    public float getShadowXOffset() {
        return shadowXOffset;
    }

    public void setShadowXOffset(float shadowXOffset) {
        this.shadowXOffset = vpToPx(shadowXOffset);
        onMeasure();
        invalidate();
        updateAnimationParam();
        HiLog.info(TAG,"Changed the Action Button shadow X offset to: "+ getShadowXOffset());
    }

    public float getShadowYOffset() {
        return shadowYOffset;
    }

    public void setShadowYOffset(float shadowYOffset) {
        this.shadowYOffset = vpToPx(shadowYOffset);
        onMeasure();
        invalidate();
        updateAnimationParam();
        HiLog.info(TAG,"Changed the Action Button shadow Y offset to: "+ getShadowYOffset());
    }

    /**
     * Returns <b>Action Button</b> shadow color
     *
     * @return <b>Action Button</b> shadow color
     */
    public int getShadowColor() {
        return shadowColor;
    }

    /**
     * Sets the <b>Action Button</b> shadow color and
     * invalidates the view
     *
     * @param shadowColor <b>Action Button</b> color
     */
    public void setShadowColor(int shadowColor) {
        this.shadowColor = shadowColor;
        invalidate();
    }

    public boolean isShadowResponsiveEffectEnabled() {
        return shadowResponsiveEffectEnabled;
    }

    public void setShadowResponsiveEffectEnabled(boolean shadowResponsiveEffectEnabled) {
        this.shadowResponsiveEffectEnabled = shadowResponsiveEffectEnabled;
        onMeasure();
        invalidate();
        HiLog.info(TAG,"the Shadow Responsive Effect = " + (isShadowResponsiveEffectEnabled() ? "Enabled" : "Disabled"));
    }

    /**
     * Returns the <b>Action Button</b> stroke width in actual
     * pixels (px)
     *
     * @return <b>Action Button</b> stroke width in actual
     * pixels (px)
     */
    public float getStrokeWidth() {
        return strokeWidth;
    }

    /**
     * Checks whether <b>Action Button</b> has stroke by checking
     * stroke width
     *
     * @return true if <b>Action Button</b> has stroke, otherwise false
     */
    public boolean hasStroke() {
        return getStrokeWidth() > 0.0f;
    }

    /**
     * Sets the <b>Action Button</b> stroke width and
     * invalidates the layout of the view
     * <p>
     * Stroke width value must be greater than 0. If stroke width is
     * set to 0 stroke is removed
     * <p>
     * Must be specified in virtual pixels (vp), which are
     * then converted into actual pixels (px)
     *
     * @param strokeWidth stroke width specified in virtual pixels (vp)     *
     */
    public void setStrokeWidth(float strokeWidth) {
        this.strokeWidth = vpToPx(strokeWidth);
        invalidate();
        updateAnimationParam();
        HiLog.info(TAG,"Changed the stroke width to: "+ getStrokeWidth());
    }

    /**
     * Removes the <b>Action Button</b> stroke by setting its width to 0
     */
    public void removeStroke() {
        if (hasStroke()) {
            setStrokeWidth(0.0f);
        }
    }

    /**
     * Returns the <b>Action Button</b> stroke color
     *
     * @return <b>Action Button</b> stroke color
     */
    public int getStrokeColor() {
        return strokeColor;
    }

    /**
     * Sets the <b>Action Button</b> stroke color and
     * invalidates the view
     *
     * @param strokeColor <b>Action Button</b> stroke color
     */
    public void setStrokeColor(int strokeColor) {
        this.strokeColor = strokeColor;
        invalidate();
        HiLog.info(TAG,"Changed the stroke color to: "+ getStrokeColor());
    }

    public PixelMap getImage() {
        return image;
    }

    public boolean hasImage() {
        return getImage() != null;
    }

    public void setImageElement(PixelMapElement image) {
        if (null != image) {
            this.image = image.getPixelMap();
        }
        else {
            this.image = null;
        }

        invalidate();
        HiLog.info(TAG, "Set the Action Button image Element");
    }

    public void setImageResource(int resId) {
        try {
            Resource resource = getResourceManager().getResource(resId);
            ImageSource imageSource = ImageSource.create(resource, null);
            PixelMap pixelMap = imageSource.createPixelmap(new ImageSource.DecodingOptions());
            setImagePixelMap(pixelMap);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void setImagePixelMap(PixelMap pixelMap){
        this.image = pixelMap;
        invalidate();
        HiLog.info(TAG, "Set the Action Button image PixelMap");
    }

    public void removeImage() {
        if (hasImage()) {
            setImageElement(null);
        }
    }

    /**
     * Returns the <b>Action Button</b> image size in actual pixels (px).
     * If <b>Action Button</b> image is not set returns 0
     *
     * @return <b>Action Button</b> image size in actual pixels (px),
     * 0 if image is not set
     */
    public float getImageSize() {
        return getImage() != null ? imageSize : 0.0f;
    }

    /**
     * Sets the size of the <b>Action Button</b> image
     *
     * Must be specified in virtual pixels (vp), which are
     * then converted into actual pixels (px)
     *
     * @param size size of the <b>Action Button</b> image
     *             specified in virtual pixels (vp)
     */
    public void setImageSize(float size) {
        this.imageSize = vpToPx(size);
    }

    public AnimatorGroup getShowAnimation() {
        return showAnimation;
    }

    public void setShowAnimation(AnimatorGroup animation) {
        showAnimation = animation;
        HiLog.info(TAG, "Set the Action Button show animation");
    }

    private AnimatorGroup createAnimatorProperty(Animations animation) {
        if (startAnimationX == 0 && startAnimationY == 0) {
            //容错布局计算尚未完成，无起始坐标
            return null;
        }

        AnimatorGroup animatorGroup = new AnimatorGroup();
        switch (animation) {
            case NONE: {
                HiLog.info(TAG, "set animation = None, startAnimationX = "+startAnimationX+", startAnimationY="+startAnimationY);
                animatorGroup.runSerially(createAnimatorProperty().setDuration(1).alpha(1).moveToY(startAnimationY).moveToX(startAnimationX).scaleY(1).scaleX(1));
                break;
            }
            case FADE_IN: {
                HiLog.info(TAG, "set animation = FADE_IN, startAnimationX = "+startAnimationX+", startAnimationY="+startAnimationY);
                animatorGroup.runSerially(createAnimatorProperty().setDuration(500).alphaFrom(0).alpha(1).moveToY(startAnimationY).moveToX(startAnimationX).scaleY(1).scaleX(1));

                break;
            }
            case FADE_OUT: {
                HiLog.info(TAG, "set animation = FADE_OUT, startAnimationX = "+startAnimationX+", startAnimationY="+startAnimationY);
                animatorGroup.runSerially(createAnimatorProperty().setDuration(300).alphaFrom(1).alpha(0).moveToY(startAnimationY).moveToX(startAnimationX).scaleY(1).scaleX(1));

                break;
            }
            case SCALE_UP: {
                animatorGroup.runSerially(createAnimatorProperty().setDuration(400).scaleXFrom(0).scaleX(1.1f).scaleYFrom(0).scaleY(1.1f).moveToY(startAnimationY).moveToX(startAnimationX).alpha(1),
                        createAnimatorProperty().setDuration(100).scaleXFrom(1.1f).scaleX(0.9f).scaleYFrom(1.1f).scaleY(0.9f));
                break;
            }
            case SCALE_DOWN: {
                animatorGroup.runSerially(createAnimatorProperty().setDuration(200).scaleXFrom(1.0f).scaleX(1.1f).scaleYFrom(1.0f).scaleY(1.1f).moveToY(startAnimationY).moveToX(startAnimationX).alpha(1),
                        createAnimatorProperty().setDuration(300).scaleXFrom(1.1f).scaleX(0).scaleYFrom(1.1f).scaleY(0));
                break;
            }
            case ROLL_FROM_RIGHT: {
                float instance = getWidth()+getStrokeWidth()+getMarginRight()+getPaddingRight();
                HiLog.info(TAG, "set animation = ROLL_FROM_RIGHT, startAnimationX = "+startAnimationX+", startAnimationY="+startAnimationY);
                animatorGroup.runSerially(createAnimatorProperty().setDuration(400).moveFromX(startAnimationX+instance).moveToX(startAnimationX-20).moveToY(startAnimationY).alpha(1).scaleY(1).scaleX(1),
                        createAnimatorProperty().setDuration(100).moveFromX(startAnimationX-20).moveToX(startAnimationX));
                break;
            }
            case ROLL_TO_RIGHT: {
                float instance = getWidth()+getStrokeWidth()+getMarginRight()+getPaddingRight();
                HiLog.info(TAG, "set animation = ROLL_TO_RIGHT, startAnimationX = "+startAnimationX+", startAnimationY="+startAnimationY);
                animatorGroup.runSerially(createAnimatorProperty().setDuration(100).moveFromX(startAnimationX).moveToX(startAnimationX-20).moveToY(startAnimationY).alpha(1).scaleY(1).scaleX(1),
                        createAnimatorProperty().setDuration(200).moveFromX(startAnimationX-20).moveToX(startAnimationX+instance));
                break;
            }
            case ROLL_FROM_DOWN: {
                HiLog.info(TAG, "set animation = ROLL_FROM_DOWN, startAnimationX = "+startAnimationX+", startAnimationY="+startAnimationY);
                float instance = getHeight()+getMarginBottom()+getPaddingBottom();
                animatorGroup.runSerially(createAnimatorProperty().setDuration(400).moveFromY(startAnimationY+instance).moveToY(startAnimationY-20).moveToX(startAnimationX).alpha(1).scaleY(1).scaleX(1),
                        createAnimatorProperty().setDuration(100).moveFromY(startAnimationY-20).moveToY(startAnimationY));
                break;
            }
            case ROLL_TO_DOWN: {
                HiLog.info(TAG, "set animation = ROLL_TO_DOWN, startAnimationX = "+startAnimationX+", startAnimationY="+startAnimationY);
                float instance = getHeight()+getMarginBottom()+getPaddingBottom();
                animatorGroup.runSerially(createAnimatorProperty().setDuration(100).moveFromY(startAnimationY).moveToY(startAnimationY-20).moveToX(startAnimationX).alpha(1).scaleY(1).scaleX(1),
                        createAnimatorProperty().setDuration(200).moveFromY(startAnimationY-20).moveToY(startAnimationY+instance));
                break;
            }
            case JUMP_FROM_RIGHT: {
                float instance = getWidth()+getStrokeWidth()+getMarginRight()+getPaddingRight();
                animatorGroup.runParallel(createAnimatorProperty().setDuration(500).scaleXFrom(0).scaleX(1.0f).scaleYFrom(0).scaleY(1.0f),
                        createAnimatorProperty().setDuration(500).moveFromX(startAnimationX+instance).moveToX(startAnimationX).moveToY(startAnimationY).alpha(1));
                break;
            }
            case JUMP_TO_RIGHT: {
                float instance = getWidth()+getStrokeWidth()+getMarginRight()+getPaddingRight();
                animatorGroup.runParallel(createAnimatorProperty().setDuration(300).scaleXFrom(1.0f).scaleX(0).scaleYFrom(1.0f).scaleY(0),
                        createAnimatorProperty().setDuration(300).moveFromX(startAnimationX).moveToX(startAnimationX+instance).moveToY(startAnimationY).alpha(1));
                break;
            }
            case JUMP_FROM_DOWN: {
                float instance = getHeight()+getMarginBottom()+getPaddingBottom();
                animatorGroup.runParallel(createAnimatorProperty().setDuration(500).scaleXFrom(0).scaleX(1.0f).scaleYFrom(0).scaleY(1.0f),
                        createAnimatorProperty().setDuration(500).moveFromY(startAnimationY+instance).moveToY(startAnimationY).moveToX(startAnimationX).alpha(1));
                break;
            }
            case JUMP_TO_DOWN: {
                float instance = getHeight()+getMarginBottom()+getPaddingBottom();
                animatorGroup.runParallel(createAnimatorProperty().setDuration(300).scaleXFrom(1.0f).scaleX(0).scaleYFrom(1.0f).scaleY(0),
                        createAnimatorProperty().setDuration(300).moveFromY(startAnimationY).moveToY(startAnimationY+instance).moveToX(startAnimationX).alpha(1));
                break;
            }
        }

        return animatorGroup;
    }

    public void setShowAnimation(Animations animation) {
        this.showAnimations = animation;
        setShowAnimation(createAnimatorProperty(animation));
    }

    public void removeShowAnimation() {
        setShowAnimation(Animations.NONE);
        HiLog.info(TAG, "Removed the Action Button show animation");
    }

    public AnimatorGroup getHideAnimation() {
        return hideAnimation;
    }

    public void setHideAnimation(AnimatorGroup animation) {

        if (null != animation) {
            animation.setStateChangedListener(new Animator.StateChangedListener() {
                @Override
                public void onStart(Animator animator) {
                }

                @Override
                public void onStop(Animator animator) {
                }

                @Override
                public void onCancel(Animator animator) {

                }

                @Override
                public void onEnd(Animator animator) {
                    setVisibility(INVISIBLE);
                }

                @Override
                public void onPause(Animator animator) {

                }

                @Override
                public void onResume(Animator animator) {

                }
            });
        }

        hideAnimation = animation;
        HiLog.info(TAG, "Set the Action Button hide animation");
    }

    public void setHideAnimation(Animations animation) {
        hideAnimations = animation;
        setHideAnimation(createAnimatorProperty(animation));
    }

    public void removeHideAnimation() {
        setHideAnimation(Animations.NONE);
    }

    public TouchPoint getTouchPoint() {
        return touchPoint;
    }

    protected void setTouchPoint(TouchPoint point) {
        this.touchPoint = point;
    }


    protected Paint getPaint() {
        return paint;
    }

    protected final void resetPaint() {
        getPaint().reset();
        getPaint().setAntiAlias(true);
    }

    protected ViewInvalidator getInvalidator() {
        return invalidator;
    }



    @Override
    public void onDraw(Component component, Canvas canvas) {
        HiLog.info(TAG, "onDraw, x = "+getContentPositionX()+", y = "+getContentPositionY());
        onMeasure();
        HiLog.info(TAG, "set animation hasDraw = " +hasDraw);
        if (!isHidden() && hasDraw ==1) {
            startAnimationX = getContentPositionX();
            startAnimationY = getContentPositionY();
            HiLog.info(TAG, "set animation start, startAnimationX = "+startAnimationX+", y = "+startAnimationY);
        }
        hasDraw++;
        if (hasDraw > 2) {
            hasDraw = 2;
        }
        drawCircle(canvas);

        if (isRippleEffectEnabled()) {
            drawRipple(canvas);
        }
//        if (hasElevation()) {
//            drawElevation();
//        }
        if (hasStroke()) {
            drawStroke(canvas);
        }
        if (hasImage()) {
            drawImage(canvas);
        }
        getInvalidator().invalidate();
//        if (!hasDraw) {
//            hasDraw = true;
//            mContext.getUITaskDispatcher().asyncDispatch(new Runnable() {
//                @Override
//                public void run() {
//                    invalidate();
//                }
//            });
//        }


    }

    /**
     * Draws the main circle of the <b>Action Button</b> and calls
     * {@link #drawShadow()} to draw the shadow if present
     *
     * @param canvas canvas, on which circle is to be drawn
     */
    protected void drawCircle(Canvas canvas) {
        resetPaint();
        if (hasShadow()) {
            if (isShadowResponsiveEffectEnabled()) {
                shadowResponsiveDrawer.draw(canvas);
            } else {
                drawShadow();
            }
        }
        getPaint().setStyle(Paint.Style.FILL_STYLE);
        boolean rippleInProgress = isRippleEffectEnabled()
                && ((RippleEffectDrawer) rippleEffectDrawer).isDrawingInProgress();

        getPaint().setColor(new Color((getState() == State.PRESSED || rippleInProgress) ? getButtonColorPressed() : getButtonColor()));
        canvas.drawCircle(calculateCenterX(), calculateCenterY(), calculateCircleRadius(), getPaint());
        HiLog.info(TAG,"Drawn the Action Button circle, x="+calculateCenterX()+", y="+calculateCenterY()+", radius="+calculateCircleRadius());
    }

    /**
     * Calculates the X-axis center coordinate of the entire view
     *
     * @return X-axis center coordinate of the entire view
     */
    protected float calculateCenterX() {
        float centerX = getEstimatedWidth() / 2f;
        HiLog.info(TAG,"Calculated Action Button center X: "+centerX);
        return centerX;
    }

    /**
     * Calculates the Y-axis center coordinate of the entire view
     *
     * @return Y-axis center coordinate of the entire view
     */
    protected float calculateCenterY() {
        float centerY = getEstimatedHeight() / 2f;
        HiLog.info(TAG,"Calculated Action Button center Y: "+ centerY);
        return centerY;
    }

    /**
     * Calculates the radius of the main circle
     *
     * @return radius of the main circle
     */
    protected final float calculateCircleRadius() {
        float circleRadius = getSize() / 2;
        HiLog.info(TAG,"Calculated Action Button circle radius: "+ circleRadius);
        return circleRadius;
    }

    protected void drawShadow() {
        getPaint().setBlurDrawLooper(new BlurDrawLooper(shadowRadius, shadowXOffset,shadowYOffset, new Color(getShadowColor())));
        HiLog.info(TAG, "Drawn the Action Button Shadow, shadowRadius="+shadowRadius+", shadowXOffset="+shadowXOffset+", shadowYOffset="+shadowYOffset);
    }

    protected void drawRipple(Canvas canvas) {
        rippleEffectDrawer.draw(canvas);
        HiLog.info(TAG, "Drawn the Action Button Ripple Effect");
    }

//    protected void drawElevation() {
//
//    }

//    private boolean hasElevation() {
//        return getElevation() > 0.0f;
//    }

    protected void drawStroke(Canvas canvas) {
        resetPaint();
        getPaint().setStyle(Paint.Style.STROKE_STYLE);
        getPaint().setStrokeWidth(getStrokeWidth());
        getPaint().setColor(new Color(getStrokeColor()));
        canvas.drawCircle(calculateCenterX(), calculateCenterY(), calculateCircleRadius(), getPaint());
        HiLog.info(TAG, "Drawn the Action Button stroke, x="+calculateCenterX()+", y="+calculateCenterY()+", radius="+calculateCircleRadius());
    }


    protected void drawImage(Canvas canvas) {
        int startPointX = (int) (calculateCenterX() - getImageSize() / 2);
        int startPointY = (int) (calculateCenterY() - getImageSize() / 2);
        int endPointX = (int) (startPointX + getImageSize());
        int endPointY = (int) (startPointY + getImageSize());
        RectFloat rectFloat = new RectFloat(startPointX, startPointY, endPointX, endPointY);

        canvas.drawPixelMapHolderRect(new PixelMapHolder(image), rectFloat, new Paint());
        HiLog.info(TAG,"Drawn the Action Button image on canvas with coordinates: X start point = "+startPointX+", " +
                        "Y start point = "+startPointY+", X end point = "+endPointX+", Y end point = "+endPointY);
    }

    protected void onMeasure() {
        ComponentContainer.LayoutConfig layoutConfig = getLayoutConfig();
        layoutConfig.width = calculateMeasuredWidth();
        layoutConfig.height = calculateMeasuredHeight();
        setLayoutConfig(layoutConfig);
    }

    /**
     * Calculates the measured width in actual pixels for the entire view
     *
     * @return measured width in actual pixels for the entire view
     */
    public int calculateMeasuredWidth() {
        int measuredWidth = (int) (getSize() + calculateShadowWidth() + calculateStrokeWeight());
        HiLog.info(TAG,"Calculated Action Button measured width: "+ measuredWidth);
        return measuredWidth;
    }

    /**
     * Calculates the measured height in actual pixels for the entire view
     *
     * @return measured width in actual pixels for the entire view
     */
    public int calculateMeasuredHeight() {
        int measuredHeight = (int) (getSize() + calculateShadowHeight() + calculateStrokeWeight());
        HiLog.info(TAG,"Calculated Action Button measured height: "+ measuredHeight);
        return measuredHeight;
    }

    /**
     * Calculates shadow width in actual pixels
     *
     * @return shadow width in actual pixels
     */
    private int calculateShadowWidth() {
        float mShadowRadius = isShadowResponsiveEffectEnabled() ?
                ((ShadowResponsiveDrawer) shadowResponsiveDrawer).getMaxShadowRadius() : getShadowRadius();
        int shadowWidth = hasShadow() ? (int) ((mShadowRadius	+ Math.abs(getShadowXOffset())) * 2) : 0;
        HiLog.info(TAG,"Calculated Action Button shadow width: "+ shadowWidth);
        return shadowWidth;
    }

    /**
     * Calculates shadow height in actual pixels
     *
     * @return shadow height in actual pixels
     */
    private int calculateShadowHeight() {
        float mShadowRadius = isShadowResponsiveEffectEnabled() ?
                ((ShadowResponsiveDrawer) shadowResponsiveDrawer).getMaxShadowRadius() : getShadowRadius();
        int shadowHeight = hasShadow() ? (int) ((mShadowRadius + Math.abs(getShadowYOffset())) * 2) : 0;
        HiLog.info(TAG,"Calculated Action Button shadow height: "+ shadowHeight);
        return shadowHeight;
    }

    /**
     * Calculates the stroke weight in actual pixels
     * *
     * @return stroke weight in actual pixels
     */
    private int calculateStrokeWeight() {
        int strokeWeight = (int) (getStrokeWidth() * 2.0f);
        HiLog.info(TAG,"Calculated Action Button stroke width: "+ strokeWidth);
        return strokeWeight;
    }

    /**
     * Converts the density-independent value into density-dependent one
     *
     * @param vp density-independent value
     * @return density-dependent value
     */
    protected float vpToPx(float vp) {
        return AttrHelper.vp2px(vp, getContext());
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        if (isEnabled()) {
            float x = touchEvent.getPointerPosition(0).getX();
            float y = touchEvent.getPointerPosition(0).getY();
            TouchPoint point = new TouchPoint(x, y);
            boolean touchPointInsideCircle = point.isInsideCircle(calculateCenterX(), calculateCenterY(),
                    calculateCircleRadius());
            int action = touchEvent.getAction();
            switch (action) {
                case TouchEvent.PRIMARY_POINT_DOWN:
                    if (touchPointInsideCircle) {
                        setState(State.PRESSED);
                        setTouchPoint(point);
                    }
                    return true;
                case TouchEvent.PRIMARY_POINT_UP:
                    if (touchPointInsideCircle) {
                        setState(State.NORMAL);
                        getTouchPoint().reset();
                    }
                    return true;
                case TouchEvent.POINT_MOVE:
                    if (!touchPointInsideCircle
                            && getState() == State.PRESSED) {
                        setState(State.NORMAL);
                        getTouchPoint().reset();
                        return true;
                    }
                    return true;
            }
        }

        return false;
    }


    /**
     * Determines the <b>Action Button</b> types
     */
    public enum Type {

        /**
         * <b>Action Button</b> default (56dp) type
         */
        DEFAULT {
            @Override
            String getString() {
                return "DEFAULT";
            }

            @Override
            int getId() {
                return 0;
            }

            @Override
            float getSize() {
                return 56.0f;
            }
        },

        /**
         * <b>Action Button</b> mini (40dp) type
         */
        MINI {
            @Override
            int getId() {
                return 1;
            }

            @Override
            float getSize() {
                return 40.0f;
            }

            @Override
            String getString() {
                return "MINI";
            }
        },

        /**
         * <b>Action Button</b> big (72dp) type
         */
        BIG {
            @Override
            int getId() {
                return 2;
            }

            @Override
            float getSize() {
                return 72.0f;
            }

            @Override
            String getString() {
                return "BIG";
            }
        };

        /**
         * Returns an {@code id} for specific <b>Action Button</b>
         * type, which is defined in attributes
         *
         * @return {@code id} for particular <b>Action Button</b> type,
         * which is defined in attributes
         */
        abstract int getId();

        /**
         * Returns the size of the specific type of the <b>Action Button</b>
         * in density-independent pixels, which then must be converted into
         * real pixels
         *
         * @return size of the particular type of the <b>Action Button</b>
         */
        abstract float getSize();

        abstract String getString();

        /**
         * Returns the <b>Action Button</b> type for a specific {@code id}
         *
         * @param id an {@code id}, for which <b>Action Button</b> type required
         * @return <b>Action Button</b> type
         */
        static Type forId(int id) {
            for (Type type : values()) {
                if (type.getId() == id) {
                    return type;
                }
            }
            return DEFAULT;
        }

        static Type forString(String str) {
            for (Type type : values()) {
                if (type.getString().equals(str)) {
                    return type;
                }
            }
            return DEFAULT;
        }

    }

    /**
     * Determines the <b>Action Button</b> states
     */
    public enum State {

        /**
         * <b>Action Button</b> normal state
         */
        NORMAL,

        /**
         * <b>Action Button</b> pressed state
         */
        PRESSED

    }

    public enum Animations {
        NONE    ,
        FADE_IN,
        FADE_OUT,
        SCALE_UP,
        SCALE_DOWN,
        ROLL_FROM_DOWN,
        ROLL_TO_DOWN,
        ROLL_FROM_RIGHT,
        ROLL_TO_RIGHT,
        JUMP_FROM_DOWN,
        JUMP_TO_DOWN,
        JUMP_FROM_RIGHT,
        JUMP_TO_RIGHT
    }

}
