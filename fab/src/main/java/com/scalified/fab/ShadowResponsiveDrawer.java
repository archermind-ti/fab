package com.scalified.fab;

import ohos.agp.render.BlurDrawLooper;
import ohos.agp.render.Canvas;
import ohos.agp.utils.Color;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class ShadowResponsiveDrawer extends EffectDrawer {
    static final HiLogLabel TAG = new HiLogLabel(HiLog.LOG_APP, 0, "ShadowResponsiveDrawer");

    /**
     * The default factor, which is used as multiplier for determining
     * the enlargement limits of the shadow
     */
    private static final float SHADOW_RESPONSE_FACTOR = 2.75f;

    /**
     * The default step, which is used for incrementing the shadow
     * radius while drawing the Shadow Responsive Effect
     */
    private static final float SHADOW_DRAWING_STEP = 0.75f;

    /**
     * Current shadow radius
     */
    private float currentShadowRadius;

    

    /**
     * Creates the {@link ShadowResponsiveDrawer} instance
     *
     * @param actionButton <b>Action Button</b> instance
     */
    ShadowResponsiveDrawer(ActionButton actionButton) {
        super(actionButton);
        init();
    }

    /**
     * Draws the Shadow Responsive Effect
     *
     * @param canvas canvas, on which the drawing is to be performed
     */
    @Override
    void draw(Canvas canvas) {
        updateRadius();
//        getActionButton().getPaint().setShadowLayer(currentShadowRadius, getActionButton().getShadowXOffset(),
//                getActionButton().getShadowYOffset(), getActionButton().getShadowColor());

        getActionButton().getPaint().setBlurDrawLooper(new BlurDrawLooper(currentShadowRadius, getActionButton().getShadowXOffset(),
                getActionButton().getShadowYOffset(), new Color(getActionButton().getShadowColor())));
        HiLog.info(TAG,"Drawn the next Shadow Responsive Effect step, currentShadowRadius = " + currentShadowRadius);
    }

    /**
     * Initializes the {@link ShadowResponsiveDrawer} instance
     */
    private void init() {
        currentShadowRadius = getActionButton().getShadowRadius();
    }

    /**
     * Updates the {@link #currentShadowRadius} depending on the current state
     */
    void updateRadius() {
        HiLog.info(TAG, "Shadow updateRadius, max = "+getMaxShadowRadius()+", min = "+getMinShadowRadius());
        if (isPressed() && currentShadowRadius < getMaxShadowRadius()) {
            currentShadowRadius += SHADOW_DRAWING_STEP;
            getActionButton().getInvalidator().requireInvalidation();
        } else if (!isPressed() && currentShadowRadius > getMinShadowRadius()) {
            currentShadowRadius -= SHADOW_DRAWING_STEP;
            getActionButton().getInvalidator().requireInvalidation();
        } else if (!isPressed()){
            currentShadowRadius = getActionButton().getShadowRadius();
        }
        HiLog.info(TAG,"Updated Shadow Responsive Effect current radius to: "+ currentShadowRadius);
    }

    /**
     * Sets the current shadow radius
     *
     * @param currentShadowRadius current shadow radius
     */
    void setCurrentShadowRadius(float currentShadowRadius) {
        this.currentShadowRadius = currentShadowRadius;
    }

    /**
     * Returns the minimum value of the shadow radius
     * <p>
     * Shadow has the minimum radius while in
     * {@link ActionButton.State#NORMAL} state
     *
     * @return minimum shadow radius
     */
    float getMinShadowRadius() {
        return getActionButton().getShadowRadius();
    }

    /**
     * Returns the maximum value of the shadow radius
     * <p>
     * Shadow has the maximum radius while in
     * {@link ActionButton.State#PRESSED} state
     * and the Shadow Responsive Effect is fully drawn
     *
     * @return maximum value of the shadow radius
     */
    float getMaxShadowRadius() {
        return getMinShadowRadius() * SHADOW_RESPONSE_FACTOR;
    }

}
