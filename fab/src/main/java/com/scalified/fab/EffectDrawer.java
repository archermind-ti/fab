package com.scalified.fab;

import ohos.agp.render.Canvas;

/**
 * Abstract class, responsible for drawing the <b>Action Button</b> effects
 *
 */
abstract class EffectDrawer {
    /**
     * <b>Action Button</b> instance
     */
    private ActionButton actionButton;

    /**
     * Called to draw a specific effect
     *
     * @param canvas canvas, on which the drawing is to be performed
     */
    abstract void draw(Canvas canvas);

    /**
     * Overrides default constructor
     *
     * @param actionButton <b>Action Button</b> instance
     */
    EffectDrawer(ActionButton actionButton) {
        this.actionButton = actionButton;
    }

    /**
     * Returns the <b>Action Button</b> instance
     *
     * @return <b>Action Button</b> instance
     */
    ActionButton getActionButton() {
        return actionButton;
    }

    /**
     * Checks whether <b>Action Button</b> is in
     * {@link ActionButton.State#PRESSED} state
     *
     * @return true if <b>Action Button</b> is in
     *         {@link ActionButton.State#PRESSED} state,
     *         otherwise false
     */
    boolean isPressed() {
        System.out.println("Shadow isPressed = " + (actionButton.getState() == ActionButton.State.PRESSED));
        return actionButton.getState() == ActionButton.State.PRESSED;
    }
}
