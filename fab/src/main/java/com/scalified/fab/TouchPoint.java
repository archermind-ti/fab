package com.scalified.fab;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * Entity class, which contains the information about X- and Y-axis
 * coordinates of the touch point
 *
 */
public final class TouchPoint {
    static final HiLogLabel TAG = new HiLogLabel(HiLog.LOG_APP, 0, "TouchPoint");

    /**
     * Touch point X-axis coordinate
     */
    private float x;

    /**
     * Touch point Y-axis coordinate
     */
    private float y;

    /**
     * The last touch point X-axis coordinate, which was saved before reset
     */
    private float lastX;

    /**
     * The last touch point Y-axis coordinate, which was saved before reset
     */
    private float lastY;

    /**
     * Creates the {@link TouchPoint} instance
     *
     * @param x touch point X-axis coordinate
     * @param y touch point Y-axis coordinate
     */
    TouchPoint(float x, float y) {
        setX(x);
        setY(y);
    }

    /**
     * Returns the touch point X-axis coordinate
     *
     * @return touch point X-axis coordinate
     */
    public float getX() {
        return x;
    }

    /**
     * Sets the touch point X-axis coordinate and calls
     * {@link #setLastX(float)}
     *
     * @param x touch point X-axis coordinate
     */
    public void setX(float x) {
        this.x = x;
        HiLog.info(TAG,"Set touch point X-axis coordinate to: "+ getX());
        setLastX(x);
    }

    /**
     * Returns the touch point Y-axis coordinate
     *
     * @return touch point Y-axis coordinate
     */
    public float getY() {
        return y;
    }

    /**
     * Sets the touch point Y-axis coordinate and calls
     * {@link #setLastY(float)}
     *
     * @param y touch point Y-axis coordinate
     */
    public void setY(float y) {
        this.y = y;
        HiLog.info(TAG,"Set touch point Y-axis coordinate to: "+ getY());
        setLastY(y);
    }

    /**
     * Returns the last touch point X-axis coordinate, which was saved before reset
     *
     * @return last touch point X-axis coordinate, which was saved before reset
     */
    public float getLastX() {
        return lastX;
    }

    /**
     * Sets the last touch point X-axis coordinate if it is greater than zero
     *
     * @param x last touch point X-axis coordinate
     */
    public void setLastX(float x) {
        if (x > 0) {
            this.lastX = x;
            HiLog.info(TAG,"Set touch point last X-axis coordinate to: "+ getLastX());
        }
    }

    /**
     * Returns the last touch point Y-axis coordinate, which was saved before reset
     *
     * @return last touch point Y-axis coordinate, which was saved before reset
     */
    public float getLastY() {
        return lastY;
    }

    /**
     * Sets the last touch point Y-axis coordinate if it is greater than zero
     *
     * @param y last touch point Y-axis coordinate
     */
    public void setLastY(float y) {
        if (y > 0) {
            this.lastY = y;
            HiLog.info(TAG,"Set touch point last Y-axis coordinate to: "+ getLastY());
        }
    }

    /**
     * Resets the touch point X- and Y-axis coordinates to zero
     */
    void reset() {
        setX(0.0f);
        setY(0.0f);
        HiLog.info(TAG,"Reset touch point");
    }

    /**
     * Checks whether the touch point is inside the circle or not
     *
     * @param centerPointX circle X-axis center coordinate
     * @param centerPointY circle Y-axis center coordinate
     * @param radius circle radius
     *
     * @return true if touch point is inside the circle, otherwise false
     */
    boolean isInsideCircle(float centerPointX, float centerPointY, float radius) {
        double xValue = Math.pow((getX() - centerPointX), 2);
        double yValue = Math.pow((getY() - centerPointY), 2);
        double radiusValue = Math.pow(radius, 2);
        boolean touchPointInsideCircle = xValue + yValue <= radiusValue;
        HiLog.info(TAG,"Detected touch point {} inside the main circle", touchPointInsideCircle ? "IS" : "IS NOT");
        return touchPointInsideCircle;
    }

    
}
