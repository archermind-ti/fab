/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.scalified.fab;

import ohos.agp.colors.RgbColor;
import ohos.agp.utils.Color;

public final class ColorModifier {
    /**
     * Forbids creation of the {@link ColorModifier} instances
     */
    private ColorModifier() {
    }

    /**
     * Modifies the color exposure using the special factor.
     * Depending on factor the result color become lighter or darker
     * <p>
     * Factor is a float value greater than 0.
     * If factor is between 0.0 and 1.0 the result color will be darker
     * (the lesser factor is the darker result color will be).
     * If factor is greater than 1.0 the result color will be lighter
     * (the more factor is the lighter result color will be).
     * <p>
     * If factor lesser than 0 is passed, no modifications will be done
     *
     * @param color  input color to modify exposure of
     * @param factor factor, which determines the rate of exposure modification
     * @return result output color with modified exposure
     */
    public static int modifyExposure(int color, float factor) {
        float mFactor = factor >= 0f ? factor : 1.0f;
        RgbColor rgbColor = RgbColor.fromArgbInt(color);
        int[] baseColor = {rgbColor.getRed(), rgbColor.getGreen(), rgbColor.getBlue()};
        float[] hsv = rgbToHsv(baseColor);
        hsv[2] *= mFactor;

        return hsvToColor(hsv);
    }

    public static float[] rgbToHsv(int[] rgb) {
        //切割rgb数组
        int r = rgb[0];
        int g = rgb[1];
        int b = rgb[2];
        //公式运算 /255
        float r1 = r / 255f;
        float g1 = g / 255f;
        float b1 = b / 255f;
        //重新拼接运算用数组
        float[] all = {r1, g1, b1};
        float max = all[0];
        float min = all[0];
        //循环查找最大值和最小值
        for (float v : all) {
            if (max <= v) {
                max = v;
            }
            if (min >= v) {
                min = v;
            }
        }
        float cMax = max;
        float cMin = min;
        //计算差值
        float diff = cMax - cMin;
        float hue = 0f;
        //判断情况计算色调H
        if (diff == 0f) {
            hue = 0f;
        } else {
            if (Math.abs(cMax - r1) == 0) {
                hue = (((g1 - b1) / diff) % 6) * 60f;
            }
            if (Math.abs(cMax - g1) == 0) {
                hue = (((b1 - r1) / diff) + 2f) * 60f;
            }
            if (Math.abs(cMax - b1) == 0) {
                hue = (((r1 - g1) / diff) + 4f) * 60f;
            }
        }
        //计算饱和度S
        float saturation;
        if (cMax == 0f) {
            saturation = 0f;
        } else {
            saturation = diff / cMax;
        }
        //计算明度V
        return new float[]{hue, saturation, cMax};
    }

    public static int hsvToColor(float[] hsv) {
        float rgbMin, rgbMax;
        int r, g, b;
        float h = hsv[0];
        float s = hsv[1];
        float v = hsv[2];

        if (h >= 360) {
            h = 0;
        }

        rgbMax = v * 1.0f;//2.55f;
        rgbMin = rgbMax * (1000 - s) / 1000.0f;

        int i = (int) (h / 60);
        int difs = (int) (h % 60);

        float rgbAdj = (rgbMax - rgbMin) * difs / 60.0f;
        switch (i) {
            case 0:
                r = (int) rgbMax;
                g = (int) (rgbMin + rgbAdj);
                b = (int) rgbMin;
                break;

            case 1:
                r = (int) (rgbMax - rgbAdj);
                g = (int) rgbMax;
                b = (int) rgbMin;
                break;

            case 2:
                r = (int) rgbMin;
                g = (int) rgbMax;
                b = (int) (rgbMin + rgbAdj);
                break;

            case 3:
                r = (int) rgbMin;
                g = (int) (rgbMax - rgbAdj);
                b = (int) rgbMax;
                break;

            case 4:
                r = (int) (rgbMin + rgbAdj);
                g = (int) rgbMin;
                b = (int) rgbMax;
                break;

            default:        // case 5:
                r = (int) rgbMax;
                g = (int) rgbMin;
                b = (int) (rgbMax - rgbAdj);
                break;
        }

        if (r > 1000) {
            r = 1000;
        }

        if (g > 1000) {
            g = 1000;
        }

        if (b > 1000) {
            b = 1000;
        }

        return Color.argb(42, r, g, b);
    }

}
