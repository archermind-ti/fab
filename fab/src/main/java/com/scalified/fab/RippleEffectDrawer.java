package com.scalified.fab;

import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * A class responsible for drawing the <b>Action Button</b> Ripple Effect
 *
 */
public class RippleEffectDrawer extends EffectDrawer {
    static final HiLogLabel TAG = new HiLogLabel(HiLog.LOG_APP, 0, "RippleEffectDrawer");

    /**
     * Default value, which current {@link #currentRadius} is incremented by
     */
    private static final int RADIUS_INCREMENT = 5;

    /**
     * Delay, which is used to complete the ripple drawing
     */
    private static final long POST_INVALIDATION_DELAY_MS = 100;

    /**
     * Current Ripple Effect radius
     */
    private int currentRadius;

    /**
     * Creates the {@link RippleEffectDrawer} instance
     *
     * @param actionButton <b>Action Button</b> instance
     */
    RippleEffectDrawer(ActionButton actionButton) {
        super(actionButton);
    }

    /**
     * Checks whether ripple effect drawing is in progress
     *
     * @return true if ripple effect drawing is in progress, otherwise false
     */
    boolean isDrawingInProgress() {
        return currentRadius > 0 && !isDrawingFinished();
    }

    /**
     * Checks whether ripple effect drawing is finished
     *
     * @return true if ripple effect drawing is finished, otherwise false
     */
    boolean isDrawingFinished() {
        return currentRadius >= getEndRippleRadius();
    }

    /**
     * Returns the end Ripple Effect radius
     *
     * @return end Ripple Effect radius
     */
    private int getEndRippleRadius() {
        return (int) (getActionButton().calculateCircleRadius() * 2);
    }

    /**
     * Updates the Ripple Effect {@link #currentRadius}
     */
    private void updateRadius() {
        if (isPressed()) {
            if (currentRadius <= getEndRippleRadius()) {
                currentRadius += RADIUS_INCREMENT;
            }
        } else {
            if (isDrawingInProgress()) {
                currentRadius = getEndRippleRadius();
            } else if (isDrawingFinished()) {
                currentRadius = 0;
            }
        }
        HiLog.info(TAG,"Updated Ripple Effect radius to: "+ currentRadius);
    }

    /**
     * Performs the entire Ripple Effect drawing frame by frame animating the process
     * <p>
     * Calls the ActionButton postInvalidate() after each currentRadius update
     * to draw the current frame animating the ripple effect drawing
     *
     * @param canvas canvas, which the Ripple Effect is drawing on
     */
    void draw(Canvas canvas) {
        updateRadius();
        drawRipple(canvas);
        ViewInvalidator invalidator = getActionButton().getInvalidator();
        if (isDrawingInProgress()) {
            invalidator.requireInvalidation();
            HiLog.info(TAG,"Drawing Ripple Effect in progress, invalidating the Action Button");
        } else if (isDrawingFinished() && !isPressed()) {
            invalidator.requireDelayedInvalidation();
            invalidator.setInvalidationDelay(POST_INVALIDATION_DELAY_MS);
            HiLog.info(TAG,"Completed Ripple Effect drawing, posting the last invalidate");
        }
    }

    /**
     * Draws the single frame of the Ripple Effect depending on Ripple Effect
     * {@link #currentRadius}
     *
     * @param canvas canvas, which the Ripple Effect is drawing on
     */
    private void drawRipple(Canvas canvas) {
        canvas.save();
        canvas.clipPath(getCircleClipPath(), Canvas.ClipOp.INTERSECT);
        TouchPoint point = getActionButton().getTouchPoint();
        canvas.drawCircle(point.getLastX(), point.getLastY(), currentRadius, getPreparedPaint());
        canvas.restore();
    }

    /**
     * Returns the clipped path, which clips the ripple circle so that it doesn't goes beyond
     * the <b>Action Button</b> circle
     *
     * @return clipped path, which clips the ripple circle
     */
    private Path getCircleClipPath() {
        Path path = new Path();
        path.addCircle(getActionButton().calculateCenterX(), getActionButton().calculateCenterY(),
                getActionButton().calculateCircleRadius(), Path.Direction.CLOCK_WISE);
        return path;
    }

    /**
     * Returns the paint, which is prepared for Ripple Effect drawing
     *
     * @return paint, which is prepared for Ripple Effect drawing
     */
    private Paint getPreparedPaint() {
        getActionButton().resetPaint();
        Paint paint = getActionButton().getPaint();
        paint.setStyle(Paint.Style.FILL_STYLE);
        paint.setColor(new Color(getActionButton().getButtonColorRipple()));
        return paint;
    }
}
