package com.scalified.fabsample.Utils;

import java.util.concurrent.atomic.AtomicInteger;

public final class IdGenerator {
    /**
     * Forbids creation of the {@link IdGenerator} instances
     */
    private IdGenerator() {
    }

    /**
     * Stores the the generated id
     */
    private static final AtomicInteger NEXT_ID = new AtomicInteger(1);

    /**
     * Generate a value suitable for use
     * <p>
     *
     *
     * @return a generated ID value
     */
    public static int next() {
        while (true) {
            int result = NEXT_ID.get();
            int newValue = result + 1;
            if (newValue > 0x00FFFFFF) {
                newValue = 1;
            }
            if (NEXT_ID.compareAndSet(result, newValue)) {
                return result;
            }
        }
    }

}
