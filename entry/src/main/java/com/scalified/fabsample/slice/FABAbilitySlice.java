package com.scalified.fabsample.slice;

import com.scalified.fab.ActionButton;
import com.scalified.fabsample.RadioButtons;
import com.scalified.fabsample.ResourceTable;
import com.scalified.fabsample.Utils.IdGenerator;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.StateElement;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;

import ohos.agp.window.dialog.ToastDialog;
import ohos.agp.window.service.WindowManager;
import ohos.media.image.ImageSource;
import ohos.media.image.common.Rect;

import java.util.Set;

public class FABAbilitySlice extends AbilitySlice {
    private static final int SLIDER_PROGRESS_MULTIPLIER = 10;
    private static final int ACTION_BUTTON_POST_DELAY_MS = 3000;
    private static final float MOVE_DISTANCE = 100.0f;

    private ActionButton actionButton;

    private RadioContainer buttonTypeRadioGroup;
    private Slider shadowRadiusSlider;
    private Slider shadowXOffsetSlider;
    private Slider shadowYOffsetSlider;
    private Checkbox defaultIconPlusCheckBox;
    private Checkbox rippleEffectEnabledCheckBox;
    private Checkbox shadowResponsiveEffectEnabledCheckBox;
    private RadioContainer buttonColorsRadioGroup;
    private RadioContainer strokeColorRadioGroup;
    private Slider strokeWidthSlider;
    private RadioContainer buttonBehaviorRadioGroup;
    private RadioContainer animationsRadioGroup;

    private AbsButton.CheckedStateChangedListener radioListener = new AbsButton.CheckedStateChangedListener() {
        @Override
        public void onCheckedChanged(AbsButton absButton, boolean b) {
            absButton.setClickable(!b);
        }
    };



    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_fab);
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(Color.getIntColor("#7f000000"));

        ScrollView scrollView = (ScrollView)findComponentById(ResourceTable.Id_fab_activity_scroll_view);
        scrollView.enableScrollBar(Component.AXIS_Y, true);
        scrollView.setScrollbarColor(Color.GRAY);
        scrollView.setScrollbarBackgroundColor(new Color(Color.getIntColor("#ffffffff")));

        initActionButton();
        initButtonTypeRadioGroup();
        initShadowRadiusSlider();
        initShadowXOffsetSlider();
        initShadowYOffsetSlider();
        initDefaultIconPlusCheckBox();
        initShadowResponsiveEffectEnabledCheckBox();
        initRippleEffectEnabledCheckBox();
        initButtonColorsRadioGroup();
        initStrokeColorRadioGroup();
        initStrokeWidthSlider();
        initButtonBehaviorRadioGroup();
        initAnimationsRadioGroup();
    }

    private void setTitleFont(Text text) {
        Font font = new Font.Builder("titlefont")
                .setWeight(Font.BOLD)
                .build();
        text.setFont(font);
    }

    private void initActionButton() {
        actionButton = (ActionButton)findComponentById(ResourceTable.Id_fab_activity_action_button);
        actionButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                int checkId = buttonBehaviorRadioGroup.getMarkedButtonId();
                switch (checkId) {
                    case 1:
                        actionButton.hide();
                        getUITaskDispatcher().delayDispatch(new Runnable() {
                            @Override
                            public void run() {
                                actionButton.show();
                            }
                        }, ACTION_BUTTON_POST_DELAY_MS);
                        break;
                    case 2:
                        actionButton.moveUp(MOVE_DISTANCE);
                        getUITaskDispatcher().delayDispatch(new Runnable() {
                            @Override
                            public void run() {
                                actionButton.moveDown(MOVE_DISTANCE);
                            }
                        }, ACTION_BUTTON_POST_DELAY_MS);
                        break;
                    case 3:
                        actionButton.moveLeft(MOVE_DISTANCE);
                        getUITaskDispatcher().delayDispatch(new Runnable() {
                            @Override
                            public void run() {
                                actionButton.moveRight(MOVE_DISTANCE);
                            }
                        }, ACTION_BUTTON_POST_DELAY_MS);
                        break;
                    default:
                        break;
                }

            }
        });
    }

    private void initButtonTypeRadioGroup() {
        setTitleFont((Text)findComponentById(ResourceTable.Id_txt_type));
        buttonTypeRadioGroup = (RadioContainer)findComponentById(ResourceTable.Id_fab_activity_radiogroup_button_type);
        switch (actionButton.getType()) {
            case DEFAULT:
                buttonTypeRadioGroup.mark(0);
                buttonTypeRadioGroup.getComponentAt(0).setClickable(false);
                break;
            case MINI:
                buttonTypeRadioGroup.mark(1);
                break;
            case BIG:
                buttonTypeRadioGroup.mark(2);
                break;
        }
        buttonTypeRadioGroup.setMarkChangedListener(new RadioContainer.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(RadioContainer radioContainer, int i) {
                switch (i) {
                    case 0:
                        actionButton.setType(ActionButton.Type.DEFAULT);
                        break;
                    case 1:
                        actionButton.setType(ActionButton.Type.MINI);
                        break;
                    case 2:
                        actionButton.setType(ActionButton.Type.BIG);
                        break;
                }
            }
        });
        int count = buttonTypeRadioGroup.getChildCount();
        for (int i = 0 ; i < count ; i++)
        {
            ((RadioButton)buttonTypeRadioGroup.getComponentAt(i)).setCheckedStateChangedListener(radioListener);
        }

    }

    private void initShadowRadiusSlider() {
        setTitleFont((Text)findComponentById(ResourceTable.Id_txt_shadow_radius));
        shadowRadiusSlider = (Slider)findComponentById(ResourceTable.Id_fab_activity_seekbar_shadow_radius);
        final int progress = (int) (pxToVp(actionButton.getShadowRadius())
                * SLIDER_PROGRESS_MULTIPLIER);
        shadowRadiusSlider.setProgressValue(progress);
        shadowRadiusSlider.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int progress, boolean b) {
                actionButton.setShadowRadius(getSliderRealProgress(progress));
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {
                String toastText = "Shadow radius = " + getSliderRealProgress(slider.getProgress());
//                new ToastDialog(getContext())
//                        .setText(toastText)
//                        .setAlignment(LayoutAlignment.BOTTOM)
//                        .show();
                showToast(toastText);
            }
        });
    }

    private void showToast(String toastText) {
        DirectionalLayout toastLayout = (DirectionalLayout) LayoutScatter.getInstance(this)
                .parse(ResourceTable.Layout_fab_toast, null, false);
        Text text = (Text) toastLayout.findComponentById(ResourceTable.Id_msg_toast);
        text.setText(toastText);
        new ToastDialog(getContext())
                .setContentCustomComponent(toastLayout)
                .setSize(DirectionalLayout.LayoutConfig.MATCH_CONTENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT)
                .setAlignment(LayoutAlignment.BOTTOM)
                .show();
    }

    private void initShadowXOffsetSlider() {
        setTitleFont((Text)findComponentById(ResourceTable.Id_txt_shadow_x));
        shadowXOffsetSlider = (Slider)findComponentById(ResourceTable.Id_fab_activity_seekbar_shadow_x_offset);
        final int progress = (int) (pxToVp(actionButton.getShadowXOffset())
                * SLIDER_PROGRESS_MULTIPLIER + shadowXOffsetSlider.getMax() / 2f);
        shadowXOffsetSlider.setProgressValue(progress);
        shadowXOffsetSlider.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int progress, boolean b) {
                actionButton.setShadowXOffset(getRealProgress(progress, slider.getMax()));
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {
                String toastText = "Shadow X Offset = " + getRealProgress(slider.getProgress(), slider.getMax());
//                new ToastDialog(getContext())
//                        .setText(toastText)
//                        .setAlignment(LayoutAlignment.CENTER)
//                        .show();
                showToast(toastText);
            }
        });

    }

    private void initShadowYOffsetSlider() {
        setTitleFont((Text)findComponentById(ResourceTable.Id_txt_shadow_y));
        shadowYOffsetSlider = (Slider)findComponentById(ResourceTable.Id_fab_activity_seekbar_shadow_y_offset);
        final int progress = (int) (pxToVp(actionButton.getShadowYOffset())
                * SLIDER_PROGRESS_MULTIPLIER + shadowYOffsetSlider.getMax() / 2f);
        shadowYOffsetSlider.setProgressValue(progress);
        shadowYOffsetSlider.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int progress, boolean b) {
                actionButton.setShadowYOffset(getRealProgress(progress, slider.getMax()));
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {
                String toastText = "Shadow Y Offset = " + getRealProgress(slider.getProgress(), slider.getMax());
//                new ToastDialog(getContext())
//                        .setText(toastText)
//                        .setAlignment(LayoutAlignment.CENTER)
//                        .show();
                showToast(toastText);
            }
        });
    }

    private void initDefaultIconPlusCheckBox() {
        setTitleFont((Text)findComponentById(ResourceTable.Id_txt_icon));
        defaultIconPlusCheckBox = (Checkbox)findComponentById(ResourceTable.Id_fab_activity_checkbox_default_icon_plus);
        defaultIconPlusCheckBox.setChecked(actionButton.hasImage());
        defaultIconPlusCheckBox.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean isChecked) {
                if (isChecked) {
                    actionButton.setImageResource(ResourceTable.Media_fab_plus_icon);
                }else {
                    actionButton.removeImage();
                }
            }
        });
    }

    private void initRippleEffectEnabledCheckBox() {
        setTitleFont((Text)findComponentById(ResourceTable.Id_txt_effect));
        rippleEffectEnabledCheckBox = (Checkbox) findComponentById(ResourceTable.Id_fab_activity_checkbox_ripple_effect_enabled);
        rippleEffectEnabledCheckBox.setChecked(actionButton.isRippleEffectEnabled());
        rippleEffectEnabledCheckBox.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean isChecked) {
                actionButton.setRippleEffectEnabled(isChecked);
            }
        });
    }

    private void initShadowResponsiveEffectEnabledCheckBox() {
        shadowResponsiveEffectEnabledCheckBox =
                (Checkbox) findComponentById(ResourceTable.Id_fab_activity_checkbox_shadow_responsive_effect_enabled);
        shadowResponsiveEffectEnabledCheckBox.setChecked(actionButton.isShadowResponsiveEffectEnabled());
        shadowResponsiveEffectEnabledCheckBox.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean isChecked) {
                actionButton.setShadowResponsiveEffectEnabled(isChecked);
            }
        });

    }

    private void initButtonColorsRadioGroup() {
        setTitleFont((Text)findComponentById(ResourceTable.Id_txt_color));
        buttonColorsRadioGroup = (RadioContainer) findComponentById(ResourceTable.Id_fab_activity_radiogroup_colors);
        populateColorsRadioGroup(buttonColorsRadioGroup, RadioButtons.COLORS);
        buttonColorsRadioGroup.setMarkChangedListener(new RadioContainer.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(RadioContainer radioContainer, int i) {
                try {
                    RadioButtons.ColorsInfo colorsInfo = (RadioButtons.ColorsInfo) radioContainer.getComponentAt(i).getTag();
                    int buttonColor = getResourceManager().getElement(colorsInfo.getPrimaryColorResId()).getColor();
                    int buttonColorPressed = getResourceManager().getElement(colorsInfo.getSecondaryColorResId()).getColor();
                    actionButton.setButtonColor(buttonColor);
                    actionButton.setButtonColorPressed(buttonColorPressed);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        buttonColorsRadioGroup.mark(0);
        buttonColorsRadioGroup.getComponentAt(0).setClickable(false);

        int count = buttonColorsRadioGroup.getChildCount();
        for (int i = 0 ; i < count ; i++)
        {
            ((RadioButton)buttonColorsRadioGroup.getComponentAt(i)).setCheckedStateChangedListener(radioListener);
        }
    }

    private void initStrokeColorRadioGroup() {
        strokeColorRadioGroup = (RadioContainer) findComponentById(ResourceTable.Id_fab_activity_radiogroup_stroke);
        populateColorsRadioGroup(strokeColorRadioGroup, RadioButtons.STROKE_COLORS);

        strokeColorRadioGroup.setMarkChangedListener(new RadioContainer.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(RadioContainer radioContainer, int i) {
                try {
                    RadioButtons.ColorsInfo colorsInfo = (RadioButtons.ColorsInfo) radioContainer.getComponentAt(i).getTag();
                    int strokeColor = getResourceManager().getElement(colorsInfo.getPrimaryColorResId()).getColor();
                    actionButton.setStrokeColor(strokeColor);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        strokeColorRadioGroup.mark(0);
        strokeColorRadioGroup.getComponentAt(0).setClickable(false);

        int count = strokeColorRadioGroup.getChildCount();
        for (int i = 0 ; i < count ; i++)
        {
            ((RadioButton)strokeColorRadioGroup.getComponentAt(i)).setCheckedStateChangedListener(radioListener);
        }
    }

    private void populateColorsRadioGroup(RadioContainer group, Set<RadioButtons.ColorsInfo> colorsInfos) {
        for (RadioButtons.ColorsInfo colorsInfo : colorsInfos) {
            RadioButton button = createColorRadioButton(colorsInfo);
            if (null != button) {
                group.addComponent(button);
            }
        }
    }

    private void populateAnimationsRadioGroup(RadioContainer group, Set<RadioButtons.AnimationInfo> animationInfos) {
        for (RadioButtons.AnimationInfo animationInfo : animationInfos) {
            RadioButton button = createAnimationRadioButton(animationInfo);
            if (null != button) {
                group.addComponent(button);
            }
        }
    }

    private void initStrokeWidthSlider() {
        strokeWidthSlider = (Slider)findComponentById(ResourceTable.Id_fab_activity_seekbar_stroke_width);
        final int progress = (int) (pxToVp(actionButton.getStrokeWidth())
                * SLIDER_PROGRESS_MULTIPLIER);
        strokeWidthSlider.setProgressValue(progress);
        strokeWidthSlider.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int progress, boolean b) {
                actionButton.setStrokeWidth(getSliderRealProgress(progress));
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {
                String toastText = "Stroke width = " + getSliderRealProgress(slider.getProgress());
//                new ToastDialog(getContext())
//                        .setText(toastText)
//                        .setAlignment(LayoutAlignment.CENTER)
//                        .show();
                showToast(toastText);
            }
        });
    }

    private void initButtonBehaviorRadioGroup() {
        setTitleFont((Text)findComponentById(ResourceTable.Id_txt_behavior));
        buttonBehaviorRadioGroup = (RadioContainer)findComponentById(ResourceTable.Id_fab_activity_button_behavior_radiogroup);
        buttonBehaviorRadioGroup.setMarkChangedListener(new RadioContainer.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(RadioContainer radioContainer, int checkedId) {
                if (null != animationsRadioGroup) {
                    for (int i = 0; i < animationsRadioGroup.getChildCount(); i++) {
                        RadioButton button = (RadioButton) animationsRadioGroup.getComponentAt(i);
                        button.setEnabled(checkedId == 1);
                        setRadioEnableState(button, checkedId == 1);
                    }
                }
            }
        });

        buttonBehaviorRadioGroup.mark(0);
        buttonBehaviorRadioGroup.getComponentAt(0).setClickable(false);

        int count = buttonBehaviorRadioGroup.getChildCount();
        for (int i = 0 ; i < count ; i++)
        {
            ((RadioButton)buttonBehaviorRadioGroup.getComponentAt(i)).setCheckedStateChangedListener(radioListener);
        }
    }

    private void initAnimationsRadioGroup() {
        setTitleFont((Text)findComponentById(ResourceTable.Id_txt_animations));
        animationsRadioGroup = (RadioContainer)findComponentById(ResourceTable.Id_fab_activity_radiogroup_animations);
        populateAnimationsRadioGroup(animationsRadioGroup, RadioButtons.ANIMATIONS);
        animationsRadioGroup.setMarkChangedListener(new RadioContainer.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(RadioContainer radioContainer, int i) {
                RadioButtons.AnimationInfo animationInfo = (RadioButtons.AnimationInfo) animationsRadioGroup.getComponentAt(i).getTag();
                actionButton.setShowAnimation(animationInfo.getAnimationOnShow());
                actionButton.setHideAnimation(animationInfo.getAnimationOnHide());
            }
        });
        animationsRadioGroup.mark(0);
        animationsRadioGroup.getComponentAt(0).setClickable(false);

        int count = animationsRadioGroup.getChildCount();
        for (int i = 0 ; i < count ; i++)
        {
            ((RadioButton)animationsRadioGroup.getComponentAt(i)).setCheckedStateChangedListener(radioListener);
        }
    }

    private RadioButton createColorRadioButton(RadioButtons.ColorsInfo colorsInfo) {
        RadioButton button = null;
        try {
            button = new RadioButton(this);
            String text = getResourceManager().getElement(colorsInfo.getColorTextResId()).getString();
            int color = getResourceManager().getElement(colorsInfo.getPrimaryColorResId()).getColor();
            int textColor = color == getResourceManager().getElement(ResourceTable.Color_fab_material_white).getColor() ?
                    getResourceManager().getElement(ResourceTable.Color_fab_material_black).getColor() : color;
            button.setId(IdGenerator.next());
            button.setText(text);
            button.setTextColorOn(new Color(textColor));
            button.setTextColorOff(new Color(textColor));
            button.setTag(colorsInfo);
            button.setTextSize(13, Text.TextSizeType.FP);

            StateElement stateElement = new StateElement();
            ImageSource onSource = ImageSource.create(getResourceManager().getResource(ResourceTable.Media_btn_radio_on), null);
            ImageSource offSource = ImageSource.create(getResourceManager().getResource(ResourceTable.Media_btn_radio_off), null);
            ImageSource.DecodingOptions decodingOpts = new ImageSource.DecodingOptions();
            PixelMapElement checkedElement = new PixelMapElement(onSource.createPixelmap(decodingOpts));
            PixelMapElement emptyElement = new PixelMapElement(offSource.createPixelmap(decodingOpts));
            stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_CHECKED}, checkedElement);
            stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, emptyElement);
            button.setButtonElement(stateElement);


        } catch (Exception e) {
            e.printStackTrace();
        }

        return button;
    }

    private void setRadioEnableState(RadioButton button, boolean enabled) {
        try {

            StateElement stateElement = new StateElement();
            ImageSource onSource = ImageSource.create(getResourceManager().getResource(ResourceTable.Media_btn_radio_on), null);
            ImageSource offSource = ImageSource.create(getResourceManager().getResource(ResourceTable.Media_btn_radio_off), null);

            ImageSource disableOnSource = ImageSource.create(getResourceManager().getResource(ResourceTable.Media_btn_radio_on_disabled_holo_light), null);
            ImageSource disableOffSource = ImageSource.create(getResourceManager().getResource(ResourceTable.Media_btn_radio_off_disabled_holo_light),null);

            ImageSource.DecodingOptions decodingOpts = new ImageSource.DecodingOptions();
            decodingOpts.desiredRegion = new Rect(0, 0, 76, 96);

            if (button.isEnabled()) {
                PixelMapElement checkedElement = new PixelMapElement(onSource.createPixelmap(decodingOpts));
                stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_CHECKED}, checkedElement);

                PixelMapElement emptyElement = new PixelMapElement(offSource.createPixelmap(decodingOpts));
                stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, emptyElement);
                button.setTextColorOff(Color.BLACK);
                button.setTextColorOn(Color.BLACK);
            } else {
                PixelMapElement checkedElement = new PixelMapElement(disableOnSource.createPixelmap(decodingOpts));
                stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_CHECKED}, checkedElement);

                PixelMapElement emptyElement = new PixelMapElement(disableOffSource.createPixelmap(decodingOpts));
                stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, emptyElement);
                button.setTextColorOff(new Color(Color.getIntColor("#9e9e9e")));
                button.setTextColorOn(new Color(Color.getIntColor("#9e9e9e")));
            }

            button.setButtonElement(stateElement);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private RadioButton createAnimationRadioButton(RadioButtons.AnimationInfo animationInfo) {
        RadioButton button = null;
        try {
            button = new RadioButton(this);
            String text = getResourceManager().getElement(animationInfo.getAnimationTextResId()).getString();

            button.setId(IdGenerator.next());
            button.setText(text);
            button.setTag(animationInfo);
            button.setTextSize(13, Text.TextSizeType.FP);
            button.setEnabled(buttonBehaviorRadioGroup.getMarkedButtonId() == 1);
            setRadioEnableState(button, buttonBehaviorRadioGroup.getMarkedButtonId() == 1);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return button;
    }


    private float getRealProgress(int progress, int max) {
        return (float) (progress - max / 2) / SLIDER_PROGRESS_MULTIPLIER;
    }

    protected static float getSliderRealProgress(int progress) {
        return (float) progress / SLIDER_PROGRESS_MULTIPLIER;
    }

    private float pxToVp(float px) {
        int density = getResourceManager().getDeviceCapability().screenDensity/160;
        return (px/density);
    }


    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
