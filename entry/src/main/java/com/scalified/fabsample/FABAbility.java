package com.scalified.fabsample;

import com.scalified.fabsample.slice.FABAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class FABAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(FABAbilitySlice.class.getName());
    }
}
